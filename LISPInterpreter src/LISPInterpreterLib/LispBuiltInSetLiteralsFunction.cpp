#include "stdafx.h"
#include "LispBuiltInSetLiteralsFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInSetLiteralsFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg, *pSecondUnevaluatedArg, *pSecondArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("set-literals!: expects 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() < 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "set-literals!: expects 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}
	if(!pFirstArg->IsLispUserDefinedFunction())
	{
		return new LispError("set-literals!: first argument has to be a LispUserDefinedFunction");
	}

	// second
	pSecondUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->Second();
	pSecondArg = pEval->Eval(pSecondUnevaluatedArg, pEnvironment);
	// error handling
	if(pSecondArg->IsLispError())
	{
		return pSecondArg;
	}
	// second arg can be nil or LispCons
	if( !pSecondArg->IsLispNil() &&
		!pSecondArg->IsLispCons())
	{
		return new LispError("set-literals!: second argument has to be a LispCons or LispNil");
	}

	dynamic_cast<LispUserDefinedFunction*>(pFirstArg)->SetLiterals(pSecondArg);

	return pFirstArg;
}