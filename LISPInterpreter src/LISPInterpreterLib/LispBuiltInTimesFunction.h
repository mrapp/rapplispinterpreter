#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInTimesFunction : public LispBuiltInFunction
{
public:
	LispBuiltInTimesFunction() { }
	~LispBuiltInTimesFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};