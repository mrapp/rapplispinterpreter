#include "stdafx.h"
#include "LispCompiler.h"
#include "LispEvaluator.h"
#include "LispFileReader.h"
#include "LispReader.h"
#include "LispError.h"


LispCompiler::~LispCompiler()
{
	if(m_pGlobalDoItEnvironment)
	{
		delete m_pGlobalDoItEnvironment;
		m_pGlobalDoItEnvironment = NULL;
	}
}


LispObject* LispCompiler::InitializeGlobalEnvironment(const char* fileNameByteCodeCompiler)
{
	m_pGlobalDoItEnvironment = new LispEnvironment();
	return InitializeGlobalEnvironment(m_pGlobalDoItEnvironment, fileNameByteCodeCompiler);
}


LispObject* LispCompiler::InitializeGlobalEnvironment(LispEnvironment* pEnvironment, const char* fileNameByteCodeCompiler)
{
	LispObject* pResult;
	pResult = LispEvaluator::DefineBuiltInFunctionsIn(pEnvironment);
	
	if(!pResult->IsLispError())
	{
		pResult = EvaluateCode(pEnvironment, fileNameByteCodeCompiler);
	}

	return pResult;
}


LispObject* LispCompiler::EvaluateCode(LispEnvironment* pEnvironment, const char* fileNameByteCodeCompiler)
{
	LispObject *pObject, *pObjectResult;
	LispFileReader* pFileReader = new LispFileReader;
	LispReader* pReader = new LispReader;
	LispEvaluator* pEvaluator = new LispEvaluator;

	if(!pFileReader->ReadLispFile(fileNameByteCodeCompiler, m_stLibraryDefinitions))
	{
		return new LispError("LispCompiler: File bytecode-compiler.lisp could not been read!");
	}

	m_stLibraryDefinitions = pReader->ReadFromString(m_stLibraryDefinitions);

	while(m_stLibraryDefinitions.size() > 0)
	{
		pObject = pReader->ReadStream(m_stLibraryDefinitions);
#ifdef _DEBUG
		if(pObject->IsLispError())
		{
			return pObject;
		}
#endif
		pObjectResult = pEvaluator->Eval(pObject, pEnvironment);
#ifdef _DEBUG
		if(pObjectResult->IsLispError())
		{
			return pObjectResult;
		}
#endif
		pReader->SkipSeperators(m_stLibraryDefinitions);

		// Free Memory
		// delete pObjectResult
		if(	pObjectResult &&
			pObject &&
			pObject != pObjectResult &&
			!pObject->IsLispSymbol() &&					// Don't delete if pObject is LispSymbol 
			!pObject->IsLispUserDefinedFunction() &&	// or userdefined function (value of binding would be deleted)
			!pObjectResult->IsLispNil() && 
			!pObjectResult->IsLispTrue() &&
			!pObjectResult->IsLispFalse())
		{
			delete pObjectResult;
			pObjectResult = NULL;
		}
		
		// delete pObject
		if(	pObject &&
			pObject->IsLispSymbol())
		{
			LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(pObject));
			pObject = NULL;
		}
		else if(pObject &&
				!pObject->IsLispNil() && 
				!pObject->IsLispTrue() &&
				!pObject->IsLispFalse())
		{
			delete pObject;
			pObject = NULL;
		}
	}

	if(pFileReader)
	{
		delete pFileReader;
		pFileReader = NULL;
	}
	if(pReader)
	{
		delete pReader;
		pReader = NULL;
	}
	if(pEvaluator)
	{
		delete pEvaluator;
		pEvaluator = NULL;
	}

	return LispNil::GetInstance();
}