#include "stdafx.h"
#include "LispCons.h"
#include "LispSymbol.h"
#include "LispPrinter.h"


LispCons::~LispCons()
{
	if(m_pFirst)
	{
		if(m_pFirst->IsLispSymbol())
		{
			LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pFirst));
			m_pFirst = NULL;
		}
		else if(!m_pFirst->IsLispNil() && 
				!m_pFirst->IsLispTrue() &&
				!m_pFirst->IsLispFalse())
		{
			delete m_pFirst;
			m_pFirst = NULL;
		}
	}

	if(m_pRest)
	{
		if(m_pRest->IsLispSymbol())
		{
			LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pRest));
			m_pRest = NULL;
		}
		else if(!m_pRest->IsLispNil() && 
				!m_pRest->IsLispTrue() &&
				!m_pRest->IsLispFalse())
		{
			delete m_pRest;
			m_pRest = NULL;
		}
	}
}

bool LispCons::IsLispCons()
{
	return true;
}

std::string LispCons::ToString()
{
	LispPrinter lispPrinter;
	return lispPrinter.Print(this);
}

bool LispCons::Equals(LispObject* pOther)
{
	if(pOther->IsLispCons())
	{
		LispCons* pConsOther = dynamic_cast<LispCons*>(pOther);

		if(	m_pFirst->Equals(pConsOther->m_pFirst) &&
			m_pRest->Equals(pConsOther->m_pRest))
		{
			return true;
		}
	}

	return false;
}

LispObject* LispCons::Second()
{
	// rest first
	return  dynamic_cast<LispCons*>(m_pRest)->m_pFirst;
}

int LispCons::Length()
{
	if(m_pRest->IsLispNil())
	{
		return 1;
	}
	else
	{
		return dynamic_cast<LispCons*>(m_pRest)->Length() + 1;
	}
}