#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInFirstFunction : public LispBuiltInFunction
{
public:
	LispBuiltInFirstFunction() { }
	~LispBuiltInFirstFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};