#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInAtomPFunction : public LispBuiltInFunction
{
public:
	LispBuiltInAtomPFunction() { }
	~LispBuiltInAtomPFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};