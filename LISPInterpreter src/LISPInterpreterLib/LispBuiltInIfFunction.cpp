#include "stdafx.h"
#include "LispBuiltInIfFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInIfFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pUnevaluatedCond, *pCond, *pUnevaluatedIfPart, *pUnevaluatedElsePart;

	// error handling - number parts
	if(!pObject->IsLispCons())
	{
		return new LispError("if: expects 3 parts after keyword if, given 0 parts in: (if)");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 3)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "if: expects 3 parts after keyword if, given " << dynamic_cast<LispCons*>(pObject)->Length() << " parts in: (if";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		streamError << ")";

		return new LispError(streamError.str());
	}
	
	// first
	pUnevaluatedCond = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	// rest first
	pUnevaluatedIfPart = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pFirst;
	// rest rest first
	pUnevaluatedElsePart = dynamic_cast<LispCons*>((dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pRest))->m_pFirst;

	pCond = pEval->Eval(pUnevaluatedCond, pEnvironment);

	// if function always true only when pCond is LispFalse else part is executed
	if(pCond->IsLispFalse())
	{
		return pEval->Eval(pUnevaluatedElsePart, pEnvironment);
	}
	else
	{
		return pEval->Eval(pUnevaluatedIfPart, pEnvironment);
	}
}