#pragma once


#include "LispObject.h"
#include "LispCons.h"


class LispError : public LispObject
{
public:
	LispError(std::string stErrorMessage = "") : 
			m_stErrorMessage(stErrorMessage) { }
	~LispError() { }

	bool IsLispError();
	bool Equals(LispObject* pOther);

	std::string ToString();

private:
	std::string m_stErrorMessage;
};