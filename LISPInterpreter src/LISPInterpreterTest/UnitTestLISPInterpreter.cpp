#include "stdafx.h"

#include "../LISPInterpreterLib/LispReader.h"
#include "../LISPInterpreterLib/LispPrinter.h"
#include "../LISPInterpreterLib/LispCons.h"
#include "../LISPInterpreterLib/LispInteger.h"
#include "../LISPInterpreterLib/LispNil.h"
#include "../LISPInterpreterLib/LispTrue.h"
#include "../LISPInterpreterLib/LispFalse.h"
#include "../LISPInterpreterLib/LispEnvironment.h"
#include "../LISPInterpreterLib/LispEvaluator.h"
#include "../LISPInterpreterLib/LispSymbol.h"
#include "../LISPInterpreterLib/LispFileReader.h"
#include "../LISPInterpreterLib/LispCompiler.h"


using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

namespace LISPInterpreterTest
{
	[TestClass]
	public ref class UnitTest1
	{
	private:
		TestContext^ testContextInstance;
		LispReader* m_pReader;
		LispPrinter* m_pPrinter;
		LispObject* m_pObject;

	public: 
		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		property Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ TestContext
		{
			Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ get()
			{
				return testContextInstance;
			}
			System::Void set(Microsoft::VisualStudio::TestTools::UnitTesting::TestContext^ value)
			{
				testContextInstance = value;
			}
		};

		#pragma region Additional test attributes
		//
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//static void MyClassInitialize(TestContext^ testContext) {};
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//static void MyClassCleanup() {};
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//void MyTestInitialize() {};
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//void MyTestCleanup() {};
		//
		#pragma endregion 

		[TestMethod]
		void Test01ReadInteger()
		{
			m_pReader = new LispReader;
			m_pObject = NULL;

			TestInteger("   0", 0);
			TestInteger("   1234", 1234);
			TestInteger("1234", 1234);
			TestInteger("1", 1);
			TestInteger("1 ", 1);
			TestInteger("1234 ", 1234);
			TestInteger("1234)", 1234);
			TestInteger("1234(", 1234);

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
		};

		void TestInteger(std::string stStream, int iExpectedResult)
		{
			m_pObject = m_pReader->ReadStream(stStream);
			Assert::IsTrue(m_pObject->IsLispInteger());
			Assert::AreEqual<int>(dynamic_cast<LispInteger*>(m_pObject)->GetValue(), iExpectedResult);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
		};


		[TestMethod]
		void Test02aReadSymbol()
		{
			m_pReader = new LispReader;
			m_pObject = NULL;

			TestSymbol(" a", "a");
			TestSymbol("   abcd", "abcd");
			TestSymbol("abcd", "abcd");
			TestSymbol("b", "b");
			TestSymbol("b ", "b");
			TestSymbol("abcd ", "abcd");
			TestSymbol("abcd)", "abcd");
			TestSymbol("abcd(", "abcd");
			TestSymbol("a---12345", "a---12345");
			TestSymbol("++++----", "++++----");
			TestSymbol("+", "+");

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
		};

		void TestSymbol(std::string stStream, std::string stExpectedResult)
		{
			m_pObject = m_pReader->ReadStream(stStream);
			Assert::IsTrue(m_pObject->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(m_pObject)->GetValue() == stExpectedResult);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}
		};


		[TestMethod]
		void Test02bReadOthers()
		{
			std::string stTest;

			m_pReader = new LispReader;
			m_pObject = NULL;

			stTest = "nil";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispNil());

			stTest = "true";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispTrue());

			stTest = "false";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispFalse());

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
		};


		[TestMethod]
		void Test03aReadList()
		{
			m_pReader = new LispReader;
			m_pObject = NULL;

			std::string stTest;
			LispCons *pCons = NULL, *pConsFirst = NULL, *pConsRest = NULL;

			stTest = "()";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispNil());


			stTest = "( )";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispNil());


			stTest = "(     )";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispNil());


			stTest = "   (     ) ";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispNil());


			stTest = "( 1 )";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispCons());
			Assert::IsTrue(dynamic_cast<LispCons*>(m_pObject)->m_pFirst->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(m_pObject)->m_pFirst)->GetValue() == 1);
			Assert::IsTrue(dynamic_cast<LispCons*>(m_pObject)->m_pRest->IsLispNil());
	
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stTest = "( 1 2 )";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispCons());
			pCons = dynamic_cast<LispCons*>(m_pObject);
			Assert::IsTrue(pCons->m_pFirst->IsLispInteger());
			Assert::IsTrue(pCons->m_pRest->IsLispCons());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pCons->m_pFirst)->GetValue() == 1);
	
			pCons = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pCons->m_pFirst->IsLispInteger());
			Assert::IsTrue(pCons->m_pRest->IsLispNil());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pCons->m_pFirst)->GetValue() == 2);
	
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stTest = "( (1) 2 )";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispCons());
			pCons = dynamic_cast<LispCons*>(m_pObject);
			Assert::IsTrue(pCons->m_pFirst->IsLispCons());
			Assert::IsTrue(pCons->m_pRest->IsLispCons());
	
			pConsRest = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pConsRest->m_pFirst->IsLispInteger());
			Assert::IsTrue(pConsRest->m_pRest->IsLispNil());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pConsRest->m_pFirst)->GetValue() == 2);

			pConsFirst = dynamic_cast<LispCons*>(pCons->m_pFirst);
			Assert::IsTrue(pConsFirst->m_pFirst->IsLispInteger());
			Assert::IsTrue(pConsFirst->m_pRest->IsLispNil());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pConsFirst->m_pFirst)->GetValue() == 1);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// addtional tests
			stTest = "(123)";
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispCons());
			Assert::IsTrue(dynamic_cast<LispCons*>(m_pObject)->m_pFirst->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(m_pObject)->m_pFirst)->GetValue() == 123);
			Assert::IsTrue(dynamic_cast<LispCons*>(m_pObject)->m_pRest->IsLispNil());
	
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// test "(1 2 3)"
			stTest = "(1 2 3)";
	
			m_pObject = m_pReader->ReadStream(stTest);
			Assert::IsTrue(m_pObject->IsLispCons());
			pCons = dynamic_cast<LispCons*>(m_pObject);
			Assert::IsTrue(pCons->m_pFirst->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pCons->m_pFirst)->GetValue() == 1);
			Assert::IsTrue(pCons->m_pRest->IsLispCons());
			pCons = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pCons->m_pFirst->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pCons->m_pFirst)->GetValue() == 2);
			Assert::IsTrue(pCons->m_pRest->IsLispCons());
			pCons = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pCons->m_pFirst->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pCons->m_pFirst)->GetValue() == 3);
			Assert::IsTrue(pCons->m_pRest->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
		};


		[TestMethod]
		void Test03bReadQuote()
		{
			m_pReader = new LispReader;
			m_pObject = NULL;

			std::string stTest;
			LispCons *pCons = NULL;

			stTest = " 'abc ";
			m_pObject = m_pReader->ReadStream(stTest);
			// must be ( quote abc )
			Assert::IsTrue(m_pObject->IsLispCons());
			pCons = dynamic_cast<LispCons*>(m_pObject);
			Assert::IsTrue(pCons->m_pFirst->IsLispSymbol());		// first
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pCons->m_pFirst)->GetValue() == "quote");
			Assert::IsTrue(pCons->m_pRest->IsLispCons());		// rest
			pCons = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pCons->m_pFirst->IsLispSymbol());		// rest first
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pCons->m_pFirst)->GetValue() == "abc");
			Assert::IsTrue(pCons->m_pRest->IsLispNil());			// rest rest

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stTest = " '(+ 1 2 3) ";
			m_pObject = m_pReader->ReadStream(stTest);
			// must be ( quote (+ 1 2 3) )
			Assert::IsTrue(m_pObject->IsLispCons());
			pCons = dynamic_cast<LispCons*>(m_pObject);
			Assert::IsTrue(pCons->m_pFirst->IsLispSymbol());		// first
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pCons->m_pFirst)->GetValue() == "quote");
			Assert::IsTrue(pCons->m_pRest->IsLispCons());		// rest
			pCons = dynamic_cast<LispCons*>(pCons->m_pRest);
			Assert::IsTrue(pCons->m_pFirst->IsLispCons());		// rest first
			Assert::IsTrue(pCons->m_pRest->IsLispNil());			// rest rest

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
		};

		
		[TestMethod]
		void Test04Printer()
		{
			LispCons* pObject1 = NULL;
			LispCons* pObject2 = NULL;
			LispInteger* pInteger = NULL;
			std::string stStream;
			m_pPrinter = new LispPrinter();
			
			// (1 true)
			pInteger = new LispInteger(1);
			pObject2 = new LispCons( LispTrue::GetInstance(), LispNil::GetInstance() );
			pObject1 = new LispCons( pInteger, pObject2 );

			stStream = m_pPrinter->Print(pObject1);
			Assert::IsTrue(stStream == "(1 true)");

			if(pObject1)
			{
				delete pObject1;
				pObject1 = NULL;
			}

			// 23456
			pInteger = new LispInteger(23456);
			stStream = m_pPrinter->Print(pInteger);
			Assert::IsTrue(stStream == "23456");

			if(pInteger)
			{
				delete pInteger;
				pInteger = NULL;
			}

			// ()
			stStream = m_pPrinter->Print(LispNil::GetInstance());
			Assert::IsTrue(stStream == "()");

			// true
			stStream = m_pPrinter->Print(LispTrue::GetInstance());
			Assert::IsTrue(stStream == "true");

			// false
			stStream = m_pPrinter->Print(LispFalse::GetInstance());
			Assert::IsTrue(stStream == "false");

			// ((a b) true)
			LispSymbol* pChar_a = LispSymbol::NewLispSymbol("a");
			LispSymbol* pChar_b = LispSymbol::NewLispSymbol("b");
			LispCons* pSublistRest = new LispCons(pChar_b, LispNil::GetInstance());
			LispCons* pSublist = new LispCons(pChar_a, pSublistRest);
			
			pObject2 = new LispCons(LispTrue::GetInstance(), LispNil::GetInstance());
			pObject1 = new LispCons(pSublist, pObject2);

			stStream = m_pPrinter->Print(pObject1);
			Assert::IsTrue(stStream == "((a b) true)");

			if(pObject1)
			{
				delete pObject1;
				pObject1 = NULL;
			}

			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test05ReadAndPrint()
		{
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;

			ReadAndPrint(" 12345 ", "12345");
			ReadAndPrint("(1 2 3)", "(1 2 3)");
			ReadAndPrint("true");
			ReadAndPrint("false");
			ReadAndPrint("  ((((1) (2)) (3)) 4) ", "((((1) (2)) (3)) 4)");
			ReadAndPrint("((((1))))");
			ReadAndPrint("()");

			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};

		void ReadAndPrint(std::string stStream)
		{
			std::string stOutput;
			std::string stTmp = stStream;
			m_pObject = m_pReader->ReadStream(stTmp);
			
			stOutput = m_pPrinter->Print(m_pObject);
			Assert::IsTrue(stOutput == stStream);

			if(	m_pObject && 
				!m_pObject->IsLispTrue() &&
				!m_pObject->IsLispFalse() &&
				!m_pObject->IsLispNil())
			{
				delete m_pObject;
				m_pObject = NULL;
			}
		};

		void ReadAndPrint(std::string stStream, std::string stExpectedResult)
		{
			std::string stOutput;
			m_pObject = m_pReader->ReadStream(stStream);
			
			stOutput = m_pPrinter->Print(m_pObject);
			Assert::IsTrue(stOutput == stExpectedResult);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
		};

		[TestMethod]
		void Test06Eval()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispObject* pTestObject = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("abc"), new LispInteger(1));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("def"), new LispInteger(2));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xt"),  LispTrue::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xf"),  LispFalse::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			stStream = "1234";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1234);
			Assert::IsTrue(m_pPrinter->Print(pObjectResult) == "1234");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "nil";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());
			Assert::IsTrue(m_pPrinter->Print(pObjectResult) == "()");

			stStream = "true";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());
			Assert::IsTrue(m_pPrinter->Print(pObjectResult) == "true");

			stStream = "false";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());
			Assert::IsTrue(m_pPrinter->Print(pObjectResult) == "false");


			stStream = "abc";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1);
			
			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			stStream = "( - 1 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == -1);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream = "( + 1 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream = "( * 10 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 20);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream = "( + 1 ( - 10 5))";
			pTestObject = new LispInteger(6);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 6);
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->Equals(pTestObject));

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}
			if(pTestObject)
			{
				delete pTestObject;
				pTestObject = NULL;
			}


			stStream = "( + abc def 1)";
			pTestObject = new LispInteger(4);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 4);
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->Equals(pTestObject));

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}
			if(pTestObject)
			{
				delete pTestObject;
				pTestObject = NULL;
			}


			stStream = "( + ( - 10 5) ( - 10 4))";
			pTestObject = new LispInteger(11);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 11);
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->Equals(pTestObject));

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}
			if(pTestObject)
			{
				delete pTestObject;
				pTestObject = NULL;
			}


			stStream = "( first (cons abc def))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "( rest (cons abc def))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "( if true 1 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream = "( if xt 1 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "( if false 1 2)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream = "( if xf abc def)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "( if ( if ( if true false true) false true) abc def)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1);
			
			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "(define xyz (+ 1 2))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "xyz";
			LispObject* pObjectxyz = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(pObjectxyz, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);


			stStream = "(define vwx (+ xyz xyz))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "vwx";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 6);
			
			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			if(pObjectxyz)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(pObjectxyz));
			}


			stStream = "(eq? 0 0)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "(eq? 0 1)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "(eq? true true)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "(eq? 0 true)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};

		[TestMethod]
		void Test07Eval()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("abc"), new LispInteger(1));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("def"), new LispInteger(2));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xt"),  LispTrue::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xf"),  LispFalse::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			stStream = "(begin 1 2 3)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(define xyz (+ 1 2))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(define vwx (+ xyz xyz))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(lambda (a b c) (+ a (* b c)))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(define add1 (lambda (a) (+ a 1)))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(define add1ToMany (lambda (a b c) (+ a b c 1)))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "add1";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			stStream = "(add1ToMany abc def 10)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 14);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(add1 abc)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(add1 (add1 (add1 (add1 abc))))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 5);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream = "(define a 999)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 999);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			stStream = "(add1 a)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream = "(add1 (+ (add1 a) (add1 a)))";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			int result = dynamic_cast<LispInteger*>(pObjectResult)->GetValue();
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2001);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test07Eval_b()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("abc"), new LispInteger(1));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("def"), new LispInteger(2));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xt"),  LispTrue::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xf"),  LispFalse::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define bla (lambda (a) \n" 
						"	(begin \n"
						"		1 \n"
						"		2 )))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			stStream =	"(bla 10)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define bla (lambda (a) 1 2 ))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispError());

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}

			stStream =	"(define bla2 (lambda (a) 1 2 ))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			stStream =	"(bla2 10)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test08Eval()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("abc"), new LispInteger(1));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("def"), new LispInteger(2));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xt"),  LispTrue::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("xf"),  LispFalse::GetInstance());
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define innerDemo \n"
						"	(lambda (a) \n"
						"		(lambda (b) \n"
						"			(+ a b)))) \n";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(innerDemo 10)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define myAdder (innerDemo 10))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(myAdder 20)";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);		// Fehler: a ist im Environment nicht vorhanden
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 30);

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test09EvalQuote()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			stStream =	"(quote a)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pObjectResult)->GetValue() == "a");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"'a";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pObjectResult)->GetValue() == "a");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(quote 12345)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 12345);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(quote (+ a (* b 6)))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispCons());
			Assert::IsTrue(dynamic_cast<LispCons*>(pObjectResult)->m_pFirst->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(dynamic_cast<LispCons*>(pObjectResult)->m_pFirst)->GetValue() == "+");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"'(+ a (* b 6))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispCons());
			Assert::IsTrue(dynamic_cast<LispCons*>(pObjectResult)->m_pFirst->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(dynamic_cast<LispCons*>(pObjectResult)->m_pFirst)->GetValue() == "+");
			stStream = m_pPrinter->Print(pObjectResult);
			Assert::IsTrue(stStream == "(+ a (* b 6))");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(print (quote helloWorld))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(begin \n"
						"	(write \"hello\") \n"
						"	(write \" \") \n"
						"	(print (quote world)) \n"
						"	(write \"\n\") )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(begin \n"
						"	(write \"hello\") \n"
						"	(write \" \") \n"
						"	(print (quote world)) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test10Set()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(set! aaaaaa 200)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispError());	// binding aaaaaa is not set


			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 100);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(set! a 200)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());


			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 200);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"( (lambda (a) \n"
						"		(begin \n"
						"			(write \"a(1) ist:\") (print a) (write \"\n\") \n"
						"			(set! a 123) \n"
						"			(write \"a(2) ist:\") (print a) (write \"\n\") \n"
						"		) \n"
						"	) 10 )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			//Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());


			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 200);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test11EvalString()
		{
			std::string stStream;
			std::string stResult;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"\"abc\"";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispString());
			stResult = "abc";
			Assert::IsTrue(dynamic_cast<LispString*>(pObjectResult)->GetValue() == stResult);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(' a)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispSymbol());
			Assert::IsTrue(dynamic_cast<LispSymbol*>(pObjectResult)->GetValue() == "a");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test12DefineFunction()
		{
			std::string stStream;
			std::string stResult;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("b"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			stStream =	"(define a (+ 1 2))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"a";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(define (add1 a) (+ a 1))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"add1";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			stStream =	"(add1 10)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 11);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(define (make-adder n) (lambda (x) (+ n x)))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define add10 (make-adder 10))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(add10 30)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 40);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(define (bodytest) 1 2 3)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(bodytest)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			int test = dynamic_cast<LispInteger*>(pObjectResult)->GetValue();
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}

			stStream =	"(define (bodytest2 a b c) (+ a 1) (+ b 2) (+ c 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(bodytest2 10 20 30)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 33);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test13ObjectWithState()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (make-point x y) \n"
			"(begin \n"
						"	(define (get-x) \n"
						"		x) \n"
						"	(define (get-y) \n"
						"		y) \n"
						"	(define (set-x! newX) \n"
						"		(set! x newX)) \n"
						"	(define (set-y! newY) \n"
						"		(set! y newY)) \n"
						"	(define (area) \n"
						"		(* x y)) \n"
						"	(define (error) \n"
						"		(print \"mist\")) \n"
						"	(define (dispatch op) \n"
						"		(if (eq? op (quote get-x)) \n"
						"			get-x \n"
						"		(if (eq? op (quote get-y)) \n"
						"			get-y \n"
						"	(if (eq? op (quote set-x!)) \n"
						"		set-x! \n"
						"	(if (eq? op (quote set-y!)) \n"
						"		set-y! \n"
						"	(if (eq? op (quote area)) \n"
						"		area \n"
						"		error)))))) \n"
						"dispatch) \n"
			")";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"make-point";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(make-point 10 20)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define p1 (make-point 10 20))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"p1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(p1 (quote area) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define (area obj) ( ( obj (quote area) )))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(area p1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 200);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test14Let()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			stStream =	"(let ((a 10) (b 20))\n"
						"	a)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 10);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(let ((a 10) (b 20))\n"
						"	(+ a b)\n"
						")";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 30);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			stStream =	"(let ((a 10) (b 20))\n"
						"	(- a 1)\n"
						"	(+ b 1)\n"
						")";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 21);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test15Predicates()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			// cons?
			stStream =	"(cons? 123)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(cons? 'a)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(cons? '())";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(cons? '(1 2 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// atom?
			stStream =	"(atom? 123)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(atom? 'a)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(atom? '())";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(atom? '(1 2 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// symbol?
			stStream =	"(symbol? 123)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(symbol? 'a)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(symbol? '())";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(symbol? '(1 2 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// number?
			stStream =	"(number? 123)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(number? 'a)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(number? '())";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(number? '(1 2 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			// integer?
			stStream =	"(integer? 123)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(integer? 'a)";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(integer? '())";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(integer? '(1 2 3))";

			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test16ReadLispFile()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());

			// test file with only one line
			Assert::IsTrue(pFileReader->ReadLispFile("..\\..\\..\\LispFiles\\test1.lisp", stStream));
			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 300);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			// test multiple lines
			Assert::IsTrue(pFileReader->ReadLispFile("..\\..\\..\\LispFiles\\makepoint.lisp", stStream));
			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"make-point";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(make-point 10 20)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define p1 (make-point 10 20))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"p1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(p1 (quote area) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(define (area obj) ( ( obj (quote area) )))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(area p1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 200);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			// test multilines and instructions
			Assert::IsTrue(pFileReader->ReadLispFile("..\\..\\..\\LispFiles\\test2.lisp", stStream));
			stStream = m_pReader->ReadFromString(stStream);

			while(stStream.size()>0)
			{
				m_pObject = m_pReader->ReadStream(stStream);
				pObjectResult = pEval->Eval(m_pObject, pEnv);

				m_pReader->SkipSeperators(stStream);
			}
			// check last operation result
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 600);

			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}

			stStream = "a";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 400);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}

			stStream = "b";
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 200);

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test17ByteVectors()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;

			m_pObject = pEnv->AddBindingFor(LispSymbol::NewLispSymbol("a"), new LispInteger(100));
			Assert::IsTrue(!m_pObject->IsLispError());
			m_pObject = pEval->DefineBuiltInFunctionsIn(pEnv);
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define v1 (make-byte-vector 10))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"v1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispByteVector());

			if(m_pObject)
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
			}


			stStream =	"(byte-vector-get v1 0)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 0);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(byte-vector-set! v1 0 255)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(byte-vector-get v1 0)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 255);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
		};


		[TestMethod]
		void Test18Compiler1()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (fac n)\n"
						"(if (eq? n 1)\n"
						"	1\n"
						"	(* n (fac (- n 1)))))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(compile-userdefined-function fac)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"fac";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(fac 10)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3628800);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test19Compiler2()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (add1 n) (+ n 1))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(compile-userdefined-function add1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"add1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(add1 100)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 101);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test20Compiler3()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (foo a b c) (+ a b (* c 2)))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(compile-userdefined-function foo)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"foo";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(foo 100 200 300)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 900);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}
			if(pObjectResult)
			{
				delete pObjectResult;
				pObjectResult = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test21Compiler4()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (foo a b) (eq? a b))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(compile-userdefined-function foo)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"foo";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(foo 100 200)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(foo 100 100)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test22Compiler5()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define (not x) (if x false true))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"(compile-userdefined-function not)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"not";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(not true)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(not false)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test23Compiler6TestFunction1()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc1
			stStream =	"(define (testfunc1 a b c) (+ a 2 b 1))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc1 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3003);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test24Compiler6TestFunction2()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc2
			stStream =	"(define (testfunc2 a b c) (+ a b) true )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc2)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc2";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc2 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test25Compiler6TestFunction3()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc3
			stStream =	"(define (testfunc3 a b c) (print \"hello\"))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc3)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc3";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc3 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispString());
			Assert::IsTrue(dynamic_cast<LispString*>(pObjectResult)->GetValue() == "hello");

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test26Compiler6TestFunction4()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());;


			//testfunc4
			stStream =	"(define (testfunc4 a b c) (print (+ 1 (+ a b) 1)) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc4)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc4";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc4 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3002);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test27Compiler6TestFunction5()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc5
			stStream =	"(define (testfunc5 a b c) (eq? a 1) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc5)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc5";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc5 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispFalse());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(testfunc5 1 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispTrue());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test28Compiler6TestFunction6()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc6
			stStream =	"(define (testfunc6 a b c) (if (eq? a 1) 10 20) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc6)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc6";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc6 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 20);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(testfunc6 1 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 10);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test29Compiler6TestFunction7()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			stStream =	"(define a 1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			//testfunc7
			stStream =	"(define (testfunc7) (+ a 2) )";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc7)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc7";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc7)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test30CompilerFirst()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfuncFirst1
			stStream =	"(define (testfuncfirst1 a b c) (first (a b)))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfuncfirst1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfuncfirst1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfuncfirst1 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 1000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			//testfuncFirst2
			stStream =	"(define (testfuncfirst2 a b c) (first ( (+ a b) c)))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfuncfirst2)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfuncfirst2";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfuncfirst2 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 3000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test31CompilerRest()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfuncrest1
			stStream =	"(define (testfuncrest1 a b c) (rest (a b)))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfuncrest1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfuncrest1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfuncrest1 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			//testfuncrest2
			stStream =	"(define (testfuncrest2 a b c) (rest (a (+ b c))))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfuncrest2)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"testfuncrest2";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfuncrest2 1000 2000 3000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 5000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		[TestMethod]
		void Test32CompilerPushNumberL()
		{
			std::string stStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfuncrest1
			stStream =	"(define (testfunc1 a b) (+ a b 3000))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc1";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc1 1000 2000)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 6000);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			//testfuncrest2
			stStream =	"(define (testfunc2 a b) (+ a b 2147483644))";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc2)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			stStream =	"testfunc2";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =	"(testfunc2 1 2)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 2147483647);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		// This test only passes in Release mode. In Debug 'System.StackOverflowException'
		[TestMethod]
		void Test33CompilerPushArgW()
		{
			std::string stStream;
			std::stringstream stStringStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc with 300 arguments
			// generate 300 arguments
			// 1 to 300
			// (define (testfunc a1 a2 ... a300) (+ a1 a300))
			stStringStream << "(define (testfunc ";

			for(int i = 1; i<=300; i++)
			{
				stStringStream << "a" << i << " ";
			}

			stStringStream << ") (+ a1 a300))";

			stStream =	stStringStream.str();

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			// clear stringstream
			stStringStream.clear();
			stStringStream.str(std::string());

			stStringStream << "(testfunc ";

			for(int i = 1; i<=300; i++)
			{
				stStringStream << i << " ";
			}

			stStringStream << ")";

			stStream =	stStringStream.str();

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 301); // a1 + a300 = 1 + 300 = 301

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};


		// This test only passes in Release mode. In Debug 'System.StackOverflowException'
		[TestMethod]
		void Test34CompilerPushArgW2()
		{
			std::string stStream;
			std::stringstream stStringStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			//testfunc with 260 arguments
			// generate 260 arguments
			// 1 to 260
			// (define (testfunc a1 a2 ... a260) (+ a1 a2 ... a260))
			// clear stringstream
			stStringStream << "(define (testfunc ";

			for(int i = 1; i<=260; i++)
			{
				stStringStream << "a" << i << " ";
			}

			stStringStream << ") (+ ";

			for(int i = 1; i<=260; i++)
			{
				stStringStream << "a" << i << " ";
			}

			stStringStream << "))";

			stStream =	stStringStream.str();

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfunc)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfunc";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			// clear stringstream
			stStringStream.clear();
			stStringStream.str(std::string());

			stStringStream << "(testfunc ";

			for(int i = 1; i<=260; i++)
			{
				stStringStream << i << " ";
			}

			stStringStream << ")";

			stStream =	stStringStream.str();

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 33930); // 1 + 2 + ... + 260 = (260*(260+1))/2 = 45150

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};

		// This test only passes in Release mode. In Debug 'System.StackOverflowException'
		[TestMethod]
		void Test35CompilerPushGlobalW()
		{
			std::string stStream;
			std::stringstream stStringStream;
			m_pReader = new LispReader;
			m_pPrinter = new LispPrinter;
			m_pObject = NULL;
			LispObject* pObjectResult = NULL;
			LispEvaluator* pEval = new LispEvaluator;
			LispEnvironment* pEnv = new LispEnvironment;
			LispFileReader *pFileReader = new LispFileReader;
			LispCompiler *pCompiler= new LispCompiler;

			m_pObject = pCompiler->InitializeGlobalEnvironment(pEnv, "..\\..\\..\\LispFiles\\bytecode-compiler.lisp");
			Assert::IsTrue(!m_pObject->IsLispError());


			// generate 300 global variables g1 to g300
			// (define ai)

			for(int i = 1; i<=300; i++)
			{
				// clear stringstream
				stStringStream.clear();
				stStringStream.str(std::string());
				stStringStream << "(define g" << i << " " << i <<")";

				stStream =	stStringStream.str();

				stStream = m_pReader->ReadFromString(stStream);
				m_pObject = m_pReader->ReadStream(stStream);
				pObjectResult = pEval->Eval(m_pObject, pEnv);
				Assert::IsTrue(pObjectResult->IsLispNil());

				if(m_pObject)
				{
					delete m_pObject;
					m_pObject = NULL;
				}

				// (compile-userdefined-function testfunci)
				// clear stringstream
				stStringStream.clear();
				stStringStream.str(std::string());
				stStringStream << "g" << i;

				stStream =	stStringStream.str();

				stStream = m_pReader->ReadFromString(stStream);
				m_pObject = m_pReader->ReadStream(stStream);
				pObjectResult = pEval->Eval(m_pObject, pEnv);
				Assert::IsTrue(pObjectResult->IsLispInteger());
				Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == i);

				if(m_pObject)
				{
					LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(m_pObject));
				}
			}


			//testfuncglobal
			// (define (testfuncglobal a) (+ a g0 ... g300)
			stStringStream.clear();
			stStringStream.str(std::string());
			stStringStream << "(define (testfuncglobal a) (+ a ";

			for(int i = 1; i<=300; i++)
			{
				stStringStream << "g" << i << " ";
			}

			stStringStream << ") )";

			stStream =	stStringStream.str();

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispNil());

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"(compile-userdefined-function testfuncglobal)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}

			stStream =	"testfuncglobal";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispUserDefinedFunction());
			Assert::IsTrue(dynamic_cast<LispUserDefinedFunction*>(pObjectResult)->m_pByteCode != LispNil::GetInstance());

			stStream =  "(testfuncglobal 1)";

			stStream = m_pReader->ReadFromString(stStream);
			m_pObject = m_pReader->ReadStream(stStream);
			pObjectResult = pEval->Eval(m_pObject, pEnv);
			Assert::IsTrue(pObjectResult->IsLispInteger());
			Assert::IsTrue(dynamic_cast<LispInteger*>(pObjectResult)->GetValue() == 45151); // 1 + 1 + 2 + ... + 300 = 1 + (300*(300+1))/2 = 45151

			if(m_pObject)
			{
				delete m_pObject;
				m_pObject = NULL;
			}


			if(pEnv)
			{
				delete pEnv;
				pEnv = NULL;
			}
			if(pEval)
			{
				delete pEval;
				pEval = NULL;
			}
			if(m_pReader)
			{
				delete m_pReader;
				m_pReader = NULL;
			}
			if(m_pPrinter)
			{
				delete m_pPrinter;
				m_pPrinter = NULL;
			}
			if(pFileReader)
			{
				delete pFileReader;
				pFileReader = NULL;
			}
		};

	};

}
