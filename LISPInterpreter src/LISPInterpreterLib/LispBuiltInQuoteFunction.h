#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInQuoteFunction : public LispBuiltInFunction
{
public:
	LispBuiltInQuoteFunction() { }
	~LispBuiltInQuoteFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};