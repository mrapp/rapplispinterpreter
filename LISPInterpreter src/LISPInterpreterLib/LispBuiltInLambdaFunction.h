#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInLambdaFunction : public LispBuiltInFunction
{
public:
	LispBuiltInLambdaFunction() { }
	~LispBuiltInLambdaFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};