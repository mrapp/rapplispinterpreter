#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInGetBodyFunction : public LispBuiltInFunction
{
public:
	LispBuiltInGetBodyFunction() { }
	~LispBuiltInGetBodyFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};