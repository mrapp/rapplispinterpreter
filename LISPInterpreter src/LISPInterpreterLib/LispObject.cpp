#include "StdAfx.h"
#include "LispObject.h"


std::string LispObject::ToString()
{
	return "";
}

bool LispObject::IsLispAtom()
{
	return false;
}

bool LispObject::IsLispNumber()
{
	return false;
}

bool LispObject::IsLispInteger()
{
	return false;
}

bool LispObject::IsLispSymbol()
{
	return false;
}

bool LispObject::IsLispCons()
{
	return false;
}

bool LispObject::IsLispNil()
{
	return false;
}


bool LispObject::IsLispTrue()
{
	return false;
}


bool LispObject::IsLispFalse()
{
	return false;
}


bool LispObject::IsLispString()
{
	return false;
}


bool LispObject::IsLispBuiltInFunction()
{
	return false;
}


bool LispObject::IsLispUserDefinedFunction()
{
	return false;
}


bool LispObject::IsLispEnvironment()
{
	return false;
}


bool LispObject::IsLispFrame()
{
	return false;
}


bool LispObject::IsLispByteVector()
{
	return false;
}


bool LispObject::IsLispError()
{
	return false;
}