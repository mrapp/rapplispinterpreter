#pragma once


#include "LispEnvironment.h"
#include "LispUserDefinedFunction.h"
#include "LispCons.h"
#include "LispFrame.h"
#include "LispByteCodeAssembler.h"

#define FRAMESTACKSIZE 1000

class LispEvaluator
{
public:
	LispEvaluator() { }
	~LispEvaluator() { }

	static LispObject* DefineBuiltInFunctionsIn(LispEnvironment* pEnvironment);

	LispObject* Eval(LispObject* pObject, LispEnvironment* pEnvironment);
	LispObject* EvalUserDefinedFunction(LispUserDefinedFunction* pLispUserDefinedFunction, LispObject* pUnevaluatedArgsInput, LispEnvironment* pEnvironment);
	LispObject* InterpretByteCode(LispByteVector* pByteCode, LispUserDefinedFunction* pLispUserDefinedFunction, LispFrame* pFrame);
};