#pragma once

#include <hash_map>
#include <vector>

#include "LispObject.h"
#include "LispCons.h"
#include "LispByteCodeConstants.h"

enum AssembleFuncEnum
{
	E_AssembleCall,
	E_AssembleEqual,
	E_AssembleFalseJmp,
	E_AssembleJump,
	E_AssembleLabel,
	E_AssembleMinus,
	E_AssemblePlus,
	E_AssemblePushArg,
	E_AssemblePushFalse,
	E_AssemblePushGlobal,
	E_AssemblePushNumber,
	E_AssemblePushTrue,
	E_AssemblePushLiteral,
	E_AssembleRetTop,
	E_AssembleTimes,
	E_AssembleTrueJmp,
	E_AssembleDrop,
	E_AssemblePrint,
	E_AssembleFirst
};

class LispByteCodeAssembler
{
public:
	std::vector<BYTE> m_ByteCodeStream;

	LispByteCodeAssembler();
	~LispByteCodeAssembler() { }

	std::vector<BYTE> AssembleCode(LispObject* pList);

private:
	std::hash_map<std::string, int> m_LabelList;	// values to back patch
	std::hash_multimap<std::string, int> m_BackPatchList;	// where to back patch. multiple back patch locations of one label is possible Vorsicht -> das ist multimap
	
	std::hash_map<std::string, AssembleFuncEnum> m_stringToFunction;
	
	void DefineAssembleFunctions();
	void AssembleInsn(LispObject* pInsn);
	void AssembleCall(LispObject* pArgs);
	void AssembleEqual(LispObject* pArgs);
	void AssembleFalseJmp(LispObject* pArgs);
	void AssembleJump(LispObject* pArgs);
	void AssembleLabel(LispObject* pArgs);
	void AssembleMinus(LispObject* pArgs);
	void AssemblePlus(LispObject* pArgs);
	void AssemblePushArg(LispObject* pArgs);
	void AssemblePushFalse(LispObject* pArgs);
	void AssemblePushGlobal(LispObject* pArgs);
	void AssemblePushNumber(LispObject* pArgs);
	void AssemblePushTrue(LispObject* pArgs);
	void AssemblePushLiteral(LispObject* pArgs);
	void AssembleRetTop(LispObject* pArgs);
	void AssembleTimes(LispObject* pArgs);
	void AssembleTrueJmp(LispObject* pArgs);
	void AssembleDrop(LispObject* pArgs);
	void AssemblePrint(LispObject* pArgs);
	void AssembleFirst(LispObject* pArgs);

	void BackPatchCode();
	void CodeForLongNumber(int iValue);
	void CodeForShortNumber(int iValue);
	void PlaceJump(LispByteCodeConstants byteCode, std::string stLabel);
	void RememberToBackPatch(int iPositionOfLabel, std::string stLabel);
};