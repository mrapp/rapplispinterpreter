#include "StdAfx.h"
#include "LispEnvironment.h"
#include "LispNil.h"
#include "LispError.h"


LispEnvironment::~LispEnvironment()
{
	DeleteAllBindings();
}


bool LispEnvironment::IsLispEnvironment()
{
	return true;
}


std::string LispEnvironment::ToString()
{
	std::stringstream ststream;

	ststream	<< "LispEnvironment\n"
				<< "\tBindings:\n";

	for(std::hash_map<LispSymbol*,LispObject*>::iterator it = m_HashMapBindings.begin(); it!=m_HashMapBindings.end(); it++)
	{
		ststream << "\t\t" << it->first->ToString() << ": " << it->second->ToString() << "\n";
	}

	ststream	<< "\tParentEnvironment:\n"
				<< m_pParentEnvironment->ToString() << "\n";

	return ststream.str();
}


bool LispEnvironment::Equals(LispObject* pOther)
{
	if(pOther->IsLispEnvironment())
	{
		LispEnvironment *pEnvironmentOther = dynamic_cast<LispEnvironment*>(pOther);

		if(	m_HashMapBindings == pEnvironmentOther->m_HashMapBindings &&
			m_pParentEnvironment &&							// check if has parent environment
			pEnvironmentOther->m_pParentEnvironment &&		// check if has parent environment
			m_pParentEnvironment->Equals(pEnvironmentOther->m_pParentEnvironment))
		{
			return true;
		}
	}
	return false;
}

// only AddBinding -> error when binding is already set
LispObject* LispEnvironment::AddBindingFor(LispSymbol* pSymbol, LispObject* pObject)
{
	std::hash_map<LispSymbol*,LispObject*>::iterator it = m_HashMapBindings.find(pSymbol);

	if(it != m_HashMapBindings.end())
	{
		// LispSymbol already exists
		return new LispError("LispEnvironment: " + pSymbol->ToString() + " already exists!");
	}
	else
	{
		// LispSymbol doesn't exist -> insert pair
		m_HashMapBindings.insert(std::pair<LispSymbol*,LispObject*>(pSymbol,pObject));

		return LispNil::GetInstance();
	}
}


LispObject* LispEnvironment::AddAllBindingsTo(std::hash_map<LispSymbol*,LispObject*> hashMapBindings)
{
	LispObject* pResult;

	for(std::hash_map<LispSymbol*,LispObject*>::iterator it = hashMapBindings.begin(); it!=hashMapBindings.end(); it++)
	{
		pResult = AddBindingFor(it->first,it->second);

		if(pResult->IsLispError())
		{
			return pResult;
		}
	}

	return LispNil::GetInstance();
}


void LispEnvironment::AddParentEnvironment(LispEnvironment* pParentEnvironment)
{
	m_pParentEnvironment = pParentEnvironment;
}


// only change binding -> error when binding is not in Environment
LispObject* LispEnvironment::ChangeBindingFor(LispSymbol* pSymbol, LispObject* pObject)
{
	std::hash_map<LispSymbol*,LispObject*>::iterator it = m_HashMapBindings.find(pSymbol);

	if(it == m_HashMapBindings.end())
	{
		// Check parent Environment
		if(m_pParentEnvironment)
		{
			return m_pParentEnvironment->ChangeBindingFor(pSymbol, pObject);
		}
		// Symbol is not in the environment
		else
		{
			// print it
			return new LispError("LispEnvironment: cannot set variable before its definition: " + pSymbol->ToString());
		}
	}
	else
	{
		// Symbol found -> change binding
		it->second = pObject;

		return LispNil::GetInstance();
	}
}


LispObject* LispEnvironment::GetBindingFor(LispSymbol* pSymbol)
{
	std::hash_map<LispSymbol*,LispObject*>::iterator it = m_HashMapBindings.find(pSymbol);

	if(it == m_HashMapBindings.end())
	{
		// Check parent Environment
		if(m_pParentEnvironment)
		{
			return m_pParentEnvironment->GetBindingFor(pSymbol);
		}
		// Symbol is not in the environment
		else
		{
			return new LispError("LispEnvironment: " + pSymbol->ToString() + " doesn't exist");
		}
	}
	else
	{
		// Symbol found
		return it->second;
	}
}


std::hash_map<LispSymbol*,LispObject*> LispEnvironment::GetAllBindings()
{
	return m_HashMapBindings;
}


void LispEnvironment::DeleteAllBindings()
{
	// delete all pointers in hashmap
	for(std::hash_map<LispSymbol*,LispObject*>::iterator it = m_HashMapBindings.begin(); it!=m_HashMapBindings.end(); it++)
	{
		if(it->second)
		{
			if(it->second->IsLispSymbol())
			{
				LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(it->second));
			}
			else if(!it->second->IsLispNil() && 
					!it->second->IsLispTrue() &&
					!it->second->IsLispFalse())
			{
				delete it->second;
				it->second = NULL;
			}
		}
		if(it->first)
		{
			LispSymbol::DeleteLispSymbol(it->first);
		}
	}
	
	// delete all bindings
	m_HashMapBindings.clear();
}