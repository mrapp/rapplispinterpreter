#include "StdAfx.h"
#include "LispFalse.h"


LispFalse* LispFalse::GetInstance()
{
	static LispFalse theInstanceFalse;
	return &theInstanceFalse;
}


bool LispFalse::IsLispFalse()
{
	return true;
}


std::string LispFalse::ToString()
{
	return "false";
}


bool LispFalse::Equals(LispObject* pOther)
{
	return pOther->IsLispFalse();
}