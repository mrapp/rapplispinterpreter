#pragma once


#include <string>

#include "LispObject.h"
#include "LispAtom.h"
#include "LispNil.h"
#include "LispTrue.h"
#include "LispFalse.h"
#include "LispInteger.h"
#include "LispSymbol.h"
#include "LispCons.h"


class LispReader
{
public:
	LispReader() { }
	~LispReader() { }

	LispObject* ReadStream(std::string& stStream);
	std::string ReadFromString(std::string stString);
	void SkipSeperators(std::string& stStream);

private:
	LispObject* ReadLispObject(std::string& stStream);
	LispObject* ReadLispList(std::string& stStream);
	LispObject* ReadLispListRest(std::string& stStream);
	LispObject* ReadLispAtom(std::string& stStream);
	LispObject* ReadQuotedExpression(std::string& stStream);
};