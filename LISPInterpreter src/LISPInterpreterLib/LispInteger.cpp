#include "StdAfx.h"
#include "LispInteger.h"


std::string LispInteger::ToString()
{
	std::stringstream stringStream;
	stringStream << m_iValue;
	return stringStream.str();
}


bool LispInteger::Equals(LispObject* pOther)
{
	if(pOther->IsLispInteger())
	{
		return m_iValue == dynamic_cast<LispInteger*>(pOther)->m_iValue;
	}
	
	return false;
}


bool LispInteger::IsLispNumber()
{
	return true;
}


bool LispInteger::IsLispInteger()
{
	return true;
}


int LispInteger::GetValue()
{
	return m_iValue;
}