#include "stdafx.h"
#include "LispBuiltInRestFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInRestFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("rest: expects 1 argument, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 1)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "rest: expects 1 argument, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);

	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}

	if(!pFirstArg->IsLispCons())
	{
		return new LispError("rest: expects argument of type <non-empty LispList>; given: " + pFirstArg->ToString());
	}

	// first(evaluted) rest
	return dynamic_cast<LispCons*>(pFirstArg)->m_pRest;
}