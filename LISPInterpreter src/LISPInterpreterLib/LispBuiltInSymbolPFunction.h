#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInSymbolPFunction : public LispBuiltInFunction
{
public:
	LispBuiltInSymbolPFunction() { }
	~LispBuiltInSymbolPFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};