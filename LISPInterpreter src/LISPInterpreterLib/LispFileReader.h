#pragma once


#include "stdafx.h"
#include <fstream>
#include "LispObject.h"
#include "LispReader.h"
#include "LispEvaluator.h"
#include "LispEnvironment.h"


class LispFileReader
{
public:
	LispFileReader() { }
	~LispFileReader() { }

	bool ReadLispFile(const char* fileName, std::string& stOutputString);

	LispObject* LoadAndExecuteLispFile(	std::string stFileName,
										std::string& stOutput,
										LispReader* pReader, 
										LispEvaluator* pEvaluator, 
										LispEnvironment* pEnvironment);
};