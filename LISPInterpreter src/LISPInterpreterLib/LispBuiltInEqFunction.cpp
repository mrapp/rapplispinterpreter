#include "stdafx.h"
#include "LispBuiltInEqFunction.h"
#include "LispCons.h"
#include "LispInteger.h"
#include "LispTrue.h"
#include "LispFalse.h"

LispObject* LispBuiltInEqFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg, *pSecondUnevaluatedArg, *pSecondArg;
	
	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("eq?: expects 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "eq?: expects 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}

	// rest first
	pSecondUnevaluatedArg = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pFirst;
	pSecondArg = pEval->Eval(pSecondUnevaluatedArg, pEnvironment);
	// error handling
	if(pSecondArg->IsLispError())
	{
		return pSecondArg;
	}

	if(pFirstArg->Equals(pSecondArg))
	{
		return LispTrue::GetInstance();
	}

	return LispFalse::GetInstance();
}