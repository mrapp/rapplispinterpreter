#include "StdAfx.h"
#include "LispByteVector.h"


LispByteVector::LispByteVector(int size)
{
	for(int i=0; i<size; i++)
	{
		m_Bytes.push_back(0);
	}
}

bool LispByteVector::IsLispByteVector()
{
	return true;
}


std::string LispByteVector::ToString()
{
	std::stringstream streamOutput;

	streamOutput << "LispByteVector( ";
	
	for (unsigned int i=0; i < m_Bytes.size(); i++)
	{
		streamOutput << m_Bytes.at(i) << ", ";
	}

	streamOutput << ")";

	return streamOutput.str();
}


bool LispByteVector::Equals(LispObject* pOther)
{
	if(pOther->IsLispByteVector())
	{
		LispByteVector *pByteVectorOther = dynamic_cast<LispByteVector*>(pOther);
		if(m_Bytes == pByteVectorOther->m_Bytes)
		{
			return true;
		}
	}

	return false;
}