#pragma once

#include "LispObject.h"
#include "LispEnvironment.h"

class LispCompiler
{
public:
	LispCompiler() : m_pGlobalDoItEnvironment(NULL) { }
	~LispCompiler();

	LispObject* InitializeGlobalEnvironment(const char* fileNameByteCodeCompiler);
	LispObject* InitializeGlobalEnvironment(LispEnvironment* pEnvironment, const char* fileNameByteCodeCompiler);

private:
	LispObject* EvaluateCode(LispEnvironment* pEnvironment, const char* fileNameByteCodeCompiler);

	LispEnvironment* m_pGlobalDoItEnvironment;
	std::string m_stLibraryDefinitions;
};