#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInIfFunction : public LispBuiltInFunction
{
public:
	LispBuiltInIfFunction() { }
	~LispBuiltInIfFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};