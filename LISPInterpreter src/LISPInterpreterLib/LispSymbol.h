#pragma once


#include <string>
#include <hash_map>
//#include <new>

#include "LispAtom.h"


class LispSymbol : public LispAtom
{
public:
	static LispSymbol* NewLispSymbol(std::string stValue);
	static void DeleteLispSymbol(LispSymbol* pSymbol);

	bool IsLispSymbol();
	std::string ToString();
	bool Equals(LispObject *pOther);

	std::string GetValue() { return m_stValue; }

private:
	LispSymbol(std::string stValue) : m_stValue(stValue), m_iReferenceCount(0) { }
	~LispSymbol() { }

	static std::hash_map<std::string,LispSymbol*> m_KnownSymbols;

	std::string m_stValue;
	int m_iReferenceCount;
};