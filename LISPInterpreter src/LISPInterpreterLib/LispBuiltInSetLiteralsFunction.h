#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInSetLiteralsFunction : public LispBuiltInFunction
{
public:
	LispBuiltInSetLiteralsFunction() { }
	~LispBuiltInSetLiteralsFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};