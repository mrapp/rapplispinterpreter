#pragma once


#include "LispAtom.h"


class LispInteger : public LispAtom
{
public:
	LispInteger(int iValue) : m_iValue(iValue) { }
	~LispInteger() { }

	std::string ToString();
	bool Equals(LispObject* pOther);

	bool IsLispNumber();
	bool IsLispInteger();
	int GetValue();

private:
	int m_iValue;
};