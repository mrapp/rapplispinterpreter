#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInWriteFunction : public LispBuiltInFunction
{
public:
	LispBuiltInWriteFunction() { }
	~LispBuiltInWriteFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};