#include "StdAfx.h"
#include "LispTrue.h"


LispTrue* LispTrue::GetInstance()
{
	static LispTrue theInstanceTrue;
	return &theInstanceTrue;
}


bool LispTrue::IsLispTrue()
{
	return true;
}


std::string LispTrue::ToString()
{
	return "true";
}


bool LispTrue::Equals(LispObject* pOther)
{
	return pOther->IsLispTrue();
}