#pragma once


#include "stdafx.h"


enum LispByteCodeConstants
{
	BCPushArgW,		// 0
	BCPushNumberB,	// 1
	BCPushNumberL,	// ...
	BCPushArgB,
	BCEqual,
	BCPlus,
	BCTimes,
	BCJumpFalse,
	BCJump,
	BCJumpTrue,
	BCPushGlobal,
	BCMinus,
	BCCall,
	BCRetTop,
	BCPushGlobalB,
	BCPushGlobalW,
	BCPushFalse,
	BCPushTrue,
	BCPushLiteralB,
	BCPushLiteralW,
	BCDrop,
	BCPrint,
	BCFirst
};