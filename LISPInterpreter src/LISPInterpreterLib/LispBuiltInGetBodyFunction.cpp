#include "stdafx.h"
#include "LispBuiltInGetBodyFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInGetBodyFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("get-body: excepts 1 argument, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 1)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "get-body: expects 1 argument, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}
	if(!pFirstArg->IsLispUserDefinedFunction())
	{
		return new LispError("get-body: argument has to be a LispUserDefinedFunction");
	}

	return dynamic_cast<LispUserDefinedFunction*>(pFirstArg)->m_pBodyList;
}