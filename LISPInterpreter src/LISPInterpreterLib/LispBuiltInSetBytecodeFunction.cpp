#include "stdafx.h"
#include "LispBuiltInSetBytecodeFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInSetBytecodeFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg, *pSecondUnevaluatedArg, *pSecondArg;

	
	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("set-bytecode!: expects 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() < 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "set-bytecode!: expects 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}
	if(!pFirstArg->IsLispUserDefinedFunction())
	{
		return new LispError("set-bytecode!: first argument has to be a LispUserDefinedFunction");
	}

	// second
	pSecondUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->Second();
	pSecondArg = pEval->Eval(pSecondUnevaluatedArg, pEnvironment);
	// error handling
	if(pSecondArg->IsLispError())
	{
		return pSecondArg;
	}
	if(!pSecondArg->IsLispCons())
	{
		return new LispError("set-bytecode!: second argument has to be a LispCons");
	}

	dynamic_cast<LispUserDefinedFunction*>(pFirstArg)->SetByteCode(pSecondArg);

	return pFirstArg;
}