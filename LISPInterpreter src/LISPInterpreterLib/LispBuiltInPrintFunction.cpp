#include "stdafx.h"
#include "LispBuiltInPrintFunction.h"
#include "LispCons.h"
#include "LispPrinter.h"
#include "LispError.h"


LispObject* LispBuiltInPrintFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispPrinter printer;
	LispObject *pFirstUnevaluatedArg, *pFirstArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("print: excepts 1 argument, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 1)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "print: expects 1 argument, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}


	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);

	if(!pFirstArg->IsLispError())
	{
		// print object
		pFirstArg->m_bPrintObject = true;
	}

	return pFirstArg;
}