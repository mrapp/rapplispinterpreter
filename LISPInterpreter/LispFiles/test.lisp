(define (add a b) (+ a b))

(define a (add 1 2))

a	; output: 5

(define b 5)

(add a b)	; output: 8