#pragma once


#include "LispObject.h"
#include "LispInteger.h"
#include "LispString.h"
#include "LispSymbol.h"
#include "LispCons.h"


class LispPrinter
{
public:
	LispPrinter() : m_bIsWriting(false) { }
	~LispPrinter() { }

	std::string Print(LispObject* pLispObject);

	void IsWriting(bool bIsWriting);
private:
	std::string PrintList(LispCons* pLispCons);
	std::string PrintListRest(LispCons* pLispCons);
	std::string PrintString(LispString* pLispString);

	bool m_bIsWriting;
};