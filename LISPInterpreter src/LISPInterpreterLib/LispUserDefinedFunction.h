#pragma once

#include <vector>

#include "LispObject.h"
#include "LispEnvironment.h"
#include "LispByteVector.h"
#include "LispNil.h"

class LispUserDefinedFunction : public LispObject
{
public:
	LispUserDefinedFunction() :	m_stFunctionName(""),
								m_pArgList(NULL), 
								m_pBodyList(NULL), 
								m_pDefiningEnvironment(NULL), 
								m_pByteCode(LispNil::GetInstance()) { }
	~LispUserDefinedFunction() { }

	bool IsLispUserDefinedFunction();
	std::string ToString();
	bool Equals(LispObject* pOther);

	void SetByteCode(LispObject* pByteCode);

	void SetLiterals(LispObject* pLiteralsList);
	LispObject* GetLiteralAt(int litIndex);

public:
	std::string m_stFunctionName;
	LispObject* m_pArgList;
	LispObject* m_pBodyList;
	LispEnvironment* m_pDefiningEnvironment;
	LispObject* m_pByteCode;

private:
	std::vector<LispObject*> m_pLiterals;
};