#include "stdafx.h"
#include "LispBuiltInQuoteFunction.h"
#include "LispCons.h"


LispObject* LispBuiltInQuoteFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("quote: excepts 1 argument, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 1)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "quote: expects 1 argument, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	return dynamic_cast<LispCons*>(pObject)->m_pFirst;
}