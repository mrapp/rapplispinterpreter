#pragma once


#include <hash_map>

#include "LispSymbol.h"
#include "LispObject.h"


class LispEnvironment : public LispObject
{
public:
	LispEnvironment() : m_pParentEnvironment(NULL) { }
	~LispEnvironment();

	bool IsLispEnvironment();
	std::string ToString();
	bool Equals(LispObject* pOther);

	LispObject* AddBindingFor(LispSymbol* pSymbol, LispObject* pObject);
	LispObject* AddAllBindingsTo(std::hash_map<LispSymbol*,LispObject*> hashMapBindings);
	void AddParentEnvironment(LispEnvironment* pParentEnvironment);
	LispObject* ChangeBindingFor(LispSymbol* pSymbol, LispObject* pObject);
	LispObject* GetBindingFor(LispSymbol* pSymbol);
	std::hash_map<LispSymbol*,LispObject*> GetAllBindings();

	void DeleteAllBindings();

private:
	std::hash_map<LispSymbol*,LispObject*> m_HashMapBindings;
	LispEnvironment* m_pParentEnvironment;
};