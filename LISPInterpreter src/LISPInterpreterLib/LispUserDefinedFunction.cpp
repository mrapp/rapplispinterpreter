#include "stdafx.h"
#include "LispUserDefinedFunction.h"
#include "LispCons.h"
#include "LispByteCodeAssembler.h"
#include "LispByteVector.h"


bool LispUserDefinedFunction::IsLispUserDefinedFunction()
{
	return true;
}


std::string LispUserDefinedFunction::ToString()
{
	return "(lambda " +	m_pArgList->ToString() + " " +
						m_pBodyList->ToString() + ")";
}

bool LispUserDefinedFunction::Equals(LispObject* pOther)
{
	if(pOther->IsLispUserDefinedFunction())
	{
		LispUserDefinedFunction* pUDFOther = dynamic_cast<LispUserDefinedFunction*>(pOther);

		if(	m_stFunctionName == pUDFOther->m_stFunctionName						&&
			m_pArgList->Equals(pUDFOther->m_pArgList)							&&
			m_pBodyList->Equals(pUDFOther->m_pBodyList)							&&
			m_pDefiningEnvironment->Equals(pUDFOther->m_pDefiningEnvironment)	&&
			m_pByteCode->Equals(pUDFOther->m_pByteCode)							&&
			m_pLiterals == pUDFOther->m_pLiterals)
		{
			return true;
		}
	}

	return false;
}


void LispUserDefinedFunction::SetByteCode(LispObject* pByteCode)
{
	LispByteCodeAssembler byteCodeAssembler;
	LispByteVector* pLispByteVector = new LispByteVector;

	pLispByteVector->m_Bytes = byteCodeAssembler.AssembleCode(pByteCode);

	m_pByteCode = pLispByteVector;
}

// Convert LispList to Vector and save it
void LispUserDefinedFunction::SetLiterals(LispObject* pLiteralsList)
{
	LispObject* pRestList;

	pRestList = pLiteralsList;

	while(!pRestList->IsLispNil())
	{
		m_pLiterals.push_back(dynamic_cast<LispCons*>(pRestList)->m_pFirst);
		pRestList = dynamic_cast<LispCons*>(pRestList)->m_pRest;
	}
}


LispObject* LispUserDefinedFunction::GetLiteralAt(int litIndex)
{
	return m_pLiterals.at(litIndex-1);
}