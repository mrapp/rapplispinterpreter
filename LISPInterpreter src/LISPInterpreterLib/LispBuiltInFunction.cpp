#include "stdafx.h"
#include "LispBuiltInFunction.h"

#include <typeinfo.h>

bool LispBuiltInFunction::IsLispBuiltInFunction()
{
	return true;
}


std::string LispBuiltInFunction::ToString()
{
	std::string stClassName = typeid(*this).name();

	return "<<procedure " + stClassName + ">>";
}


bool LispBuiltInFunction::Equals(LispObject *pOther)
{
	return this==pOther;
}