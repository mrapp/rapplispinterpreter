#include "stdafx.h"
#include "LispFileReader.h"
#include "LispError.h"
#include "LispNil.h"


bool LispFileReader::ReadLispFile(const char* fileName, std::string& stOutputString)
{
	std::string stLine;
	std::ifstream  fileInputStream(fileName);

	stOutputString = "";

	if(fileInputStream.is_open())
	{
		int iCommentPosition;
		// read file
		while(!fileInputStream.eof())
		{
			getline(fileInputStream, stLine);
			
			// empty line
			if(	stLine.size() == 0)
			{
				// skip line
				continue;
			}
			// comment line
			iCommentPosition = stLine.find_first_of(';');
			if(iCommentPosition != std::string::npos)
			{
				// start of line
				if(iCommentPosition == 0)
				{
					// skip line
					continue;
				}
				else
				{
					// delete ; and everything after ;
					stLine = stLine.substr(0,iCommentPosition);
				}
			}
			stLine.append("\n");

			stOutputString.append(stLine);
		}

		fileInputStream.close();

		return true;
	}
	else
	{
		// failed to open file
		std::cerr << "Failed to open file: " << fileName << std::endl;
		return false;
	}
}


LispObject* LispFileReader::LoadAndExecuteLispFile(std::string stFileName, std::string& stOutput, LispReader* pReader, LispEvaluator* pEvaluator, LispEnvironment* pEnvironment)
{
	LispObject *pObject, *pObjectResult;
	std::string stFileStream;

	stOutput = "";

	if(!ReadLispFile(stFileName.c_str(), stFileStream))
	{
		return new LispError("LispFileReader: File " + stFileName + " could not be read");
	}

	stFileStream = pReader->ReadFromString(stFileStream);

	while(stFileStream.size() > 0)
	{
		pObject = pReader->ReadStream(stFileStream);
		// error-handling
		if(pObject->IsLispError())
		{
			return pObject;
		}

		pObjectResult = pEvaluator->Eval(pObject, pEnvironment);
		// error-handling
		if(pObjectResult->IsLispError())
		{
			return pObjectResult;
		}

#ifdef _DEBUG
		// print nil only in debug version
		if(pObjectResult)
		{
			stOutput = stOutput + pObjectResult->ToString() + "\n";
		}
#endif

		if( pObjectResult &&
			!pObjectResult->IsLispNil())
		{
			stOutput = stOutput + pObjectResult->ToString() + "\n";
		}

		pReader->SkipSeperators(stFileStream);

		// Free Memory
		// delete pObjectResult
		if(	pObjectResult &&
			pObject &&
			pObject != pObjectResult &&
			!pObject->IsLispSymbol() &&					// Don't delete if pObject is LispSymbol.
			!pObject->IsLispUserDefinedFunction() &&	// or userdefined function (value of binding would be deleted)
			!pObjectResult->IsLispNil() && 
			!pObjectResult->IsLispTrue() &&
			!pObjectResult->IsLispFalse())
		{
			delete pObjectResult;
			pObjectResult = NULL;
		}
		
		// delete pObject
		if( pObject &&
			pObject->IsLispSymbol())
		{
			LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(pObject));
			pObject = NULL;
		}
		else if(pObject &&
				!pObject->IsLispNil() && 
				!pObject->IsLispTrue() &&
				!pObject->IsLispFalse())
		{
			delete pObject;
			pObject = NULL;
		}
	}

	return LispNil::GetInstance();
}