#include "stdafx.h"
#include "LispBuiltInSetFunction.h"
#include "LispCons.h"
#include "LispInteger.h"
#include "LispNil.h"


LispObject* LispBuiltInSetFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pUnevaluatedName, *pUnevaluatedExpr, *pEvaluatedExpr;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("set!: expects 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "set!: expects 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pUnevaluatedName = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	// error handling
	if(!pUnevaluatedName->IsLispSymbol())
	{
		return new LispError("set!: first argument has to be an identifier");
	}

	// rest first
	pUnevaluatedExpr = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pFirst;
	pEvaluatedExpr = pEval->Eval(pUnevaluatedExpr, pEnvironment);
	// error handling
	if(pEvaluatedExpr->IsLispError())
	{
		return pEvaluatedExpr;
	}

	return pEnvironment->ChangeBindingFor(dynamic_cast<LispSymbol*>(pUnevaluatedName), pEvaluatedExpr);
}