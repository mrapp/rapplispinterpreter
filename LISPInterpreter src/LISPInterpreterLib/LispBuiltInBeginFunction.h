#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInBeginFunction : public LispBuiltInFunction
{
public:
	LispBuiltInBeginFunction() { }
	~LispBuiltInBeginFunction() { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};