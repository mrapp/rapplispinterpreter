#include "stdafx.h"
#include "LispBuiltInByteVectorSetFunction.h"
#include "LispCons.h"
#include "LispNil.h"
#include "LispInteger.h"
#include "LispByteVector.h"
#include "LispError.h"


LispObject* LispBuiltInByteVectorSetFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pByteVectorArg, *pSecondUnevaluatedArg, *pIndexArg, *pThirdUnevaluatedArg, *pValueArg;
	LispByteVector *pByteVector;
	int iIndexArg, iValueArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("byte-vector-set!: expects 3 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 3)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "byte-vector-set!: expects 3 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pByteVectorArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pByteVectorArg->IsLispError())
	{
		return pByteVectorArg;
	}
	if(!pByteVectorArg->IsLispByteVector())
	{
		return new LispError("byte-vector-set!: first argument has to be a LispByteVector");
	}

	// second
	pSecondUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->Second();
	pIndexArg = pEval->Eval(pSecondUnevaluatedArg, pEnvironment);
	// error handling
	if(pIndexArg->IsLispError())
	{
		return pIndexArg;
	}
	if(!pIndexArg->IsLispInteger())
	{
		return new LispError("byte-vector-set!: index argument has to be an integer");
	}
	pByteVector = dynamic_cast<LispByteVector*>(pByteVectorArg);
	iIndexArg = dynamic_cast<LispInteger*>(pIndexArg)->GetValue();
	if(	iIndexArg < 0 ||
		iIndexArg >= static_cast<int>(pByteVector->m_Bytes.size()))
	{
		std::stringstream streamError;

		streamError << "byte-vector-set!: index argument " << iIndexArg 
					<< " is out of range, range is: 0 to " << (static_cast<int>(pByteVector->m_Bytes.size())-1);

		return new LispError(streamError.str());
	}

	// third (rest rest first)
	pThirdUnevaluatedArg = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pRest)->m_pFirst;
	pValueArg = pEval->Eval(pThirdUnevaluatedArg, pEnvironment);
	// error handling
	if(pValueArg->IsLispError())
	{
		return pValueArg;
	}
	if(!pValueArg->IsLispInteger())
	{
		return new LispError("byte-vector-set!: value argument has to be an integer");
	}
	iValueArg = dynamic_cast<LispInteger*>(pValueArg)->GetValue();
	if(!(	iValueArg >= 0 &&
			iValueArg <= 255))
	{
		return new LispError("byte-vector-set!: value argument is out of range, range is: 0 to 255");
	}

	dynamic_cast<LispByteVector*>(pByteVectorArg)->m_Bytes.at(iIndexArg) = static_cast<LispByteCodeConstants>(iValueArg);

	return LispNil::GetInstance();
}