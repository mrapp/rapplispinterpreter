#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInRestFunction : public LispBuiltInFunction
{
public:
	LispBuiltInRestFunction() { }
	~LispBuiltInRestFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};