#include "stdafx.h"
#include "LispString.h"


bool LispString::IsLispString()
{
	return true;
}

std::string LispString::ToString()
{
	std::string stOutput = "\"";
	stOutput.append(m_stValue);
	stOutput.append("\"");

	return stOutput;
}

bool LispString::Equals(LispObject* pOther)
{
	if(pOther->IsLispString())
	{
		LispString* pStringOther = dynamic_cast<LispString*>(pOther);

		if(m_stValue == pStringOther->m_stValue)
		{
			return true;
		}
	}

	return false;
}

std::string LispString::GetValue()
{
	return m_stValue;
}