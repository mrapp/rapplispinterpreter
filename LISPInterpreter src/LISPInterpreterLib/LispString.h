#pragma once


#include "LispAtom.h"


class LispString : public LispAtom
{
public:
	LispString(std::string stValue) : m_stValue(stValue) { }
	~LispString() { }

	bool IsLispString();
	bool Equals(LispObject* pOther);

	std::string GetValue();
	std::string ToString();
private:
	std::string m_stValue;
};