#include "stdafx.h"
#include "LispError.h"


bool LispError::IsLispError()
{
	return true;
}

bool LispError::Equals(LispObject* pOther)
{
	if(pOther->IsLispError())
	{
		LispError* pErrorOther = dynamic_cast<LispError*>(pOther);

		if(	m_stErrorMessage == pErrorOther->m_stErrorMessage)
		{
			return true;
		}
	}

	return false;
}


std::string LispError::ToString()
{
	if(m_stErrorMessage.length() == 0)
	{
		// no error message -> return empty string
		return "";
	}

	std::string stOutput = "LispERROR: ";
	stOutput.append(m_stErrorMessage);

	return stOutput;
}