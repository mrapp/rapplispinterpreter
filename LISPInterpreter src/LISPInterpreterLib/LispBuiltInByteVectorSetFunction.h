#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInByteVectorSetFunction : public LispBuiltInFunction
{
public:
	LispBuiltInByteVectorSetFunction() { }
	~LispBuiltInByteVectorSetFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};