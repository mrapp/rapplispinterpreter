#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInSetFunction : public LispBuiltInFunction
{
public:
	LispBuiltInSetFunction() { }
	~LispBuiltInSetFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};