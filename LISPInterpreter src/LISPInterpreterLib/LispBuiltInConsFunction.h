#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInConsFunction : public LispBuiltInFunction
{
public:
	LispBuiltInConsFunction() { }
	~LispBuiltInConsFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};