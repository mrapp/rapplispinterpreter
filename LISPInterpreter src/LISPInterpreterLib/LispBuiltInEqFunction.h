#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInEqFunction : public LispBuiltInFunction
{
public:
	LispBuiltInEqFunction() { }
	~LispBuiltInEqFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};