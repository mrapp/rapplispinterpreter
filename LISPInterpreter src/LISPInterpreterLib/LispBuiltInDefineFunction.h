#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInDefineFunction : public LispBuiltInFunction
{
public:
	LispBuiltInDefineFunction() { }
	~LispBuiltInDefineFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};