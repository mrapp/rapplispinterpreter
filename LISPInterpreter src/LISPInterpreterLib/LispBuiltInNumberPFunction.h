#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInNumberPFunction : public LispBuiltInFunction
{
public:
	LispBuiltInNumberPFunction() { }
	~LispBuiltInNumberPFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};