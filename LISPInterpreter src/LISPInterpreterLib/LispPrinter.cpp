#include "stdafx.h"
#include "LispPrinter.h"


std::string LispPrinter::Print(LispObject* pLispObject)
{
	if(pLispObject->IsLispNil())
	{
		return "()";
	}

	if(pLispObject->IsLispString())
	{
		return PrintString(dynamic_cast<LispString*>(pLispObject));
	}

	// has to be LispCons
	if(pLispObject->IsLispCons())
	{
		return PrintList(dynamic_cast<LispCons*>(pLispObject));
	}

	// LispInteger, LispSymbol, LispTrue, LispFalse, LispUserdefinedFunction
	return pLispObject->ToString();
}


void LispPrinter::IsWriting(bool bIsWriting)
{
	m_bIsWriting = bIsWriting;
}


std::string LispPrinter::PrintList(LispCons* pLispCons)
{
	return "(" + PrintListRest(pLispCons);
}


std::string LispPrinter::PrintListRest(LispCons* pLispCons)
{
	std::string stResult = Print(pLispCons->m_pFirst);

	if(pLispCons->m_pRest->IsLispNil())
	{
		return stResult + ")";
	}
	
	if(pLispCons->m_pRest->IsLispAtom())
	{
		return stResult + " " + Print(pLispCons->m_pRest) + ")";
	}

	// has to be LispCons
	return stResult + " " + PrintListRest(dynamic_cast<LispCons*>(pLispCons->m_pRest));
}


std::string LispPrinter::PrintString(LispString* pLispString)
{
	std::string stResult;
	stResult = pLispString->GetValue();
	
	if(!m_bIsWriting)
	{
		std::string stTmp = "";

		// add '\n' to string
		for (unsigned int i=0; i<stResult.length(); i++)
		{
			// check for new line
			if(stResult.at(i) == '\n')
			{
				stTmp.append("\\n");
			}
			// check for tab
			else if(stResult.at(i) == '\t')
			{
				stTmp.append("\\t");
			}
			else
			{
				stTmp.append(stResult.substr(i,1));
			}
		}

		stResult = stTmp;
	}

	return stResult;
}