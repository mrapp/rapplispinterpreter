#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInMakeByteVectorFunction : public LispBuiltInFunction
{
public:
	LispBuiltInMakeByteVectorFunction() { }
	~LispBuiltInMakeByteVectorFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};