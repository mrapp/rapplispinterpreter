#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInByteVectorGetFunction : public LispBuiltInFunction
{
public:
	LispBuiltInByteVectorGetFunction() { }
	~LispBuiltInByteVectorGetFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};