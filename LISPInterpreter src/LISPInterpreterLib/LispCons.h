#pragma once


#include "LispObject.h"


class LispCons : public LispObject
{
public:	
	LispObject* m_pFirst;
	LispObject* m_pRest;
	
	LispCons(LispObject* first, LispObject* rest) : m_pFirst(first), m_pRest(rest) { }
	~LispCons( );

	bool IsLispCons();
	std::string ToString();
	bool Equals(LispObject* pOther);

	LispObject* Second();
	int Length();
};