#include "stdafx.h"
#include "LispBuiltInLetFunction.h"
#include "LispCons.h"
#include "LispNil.h"


LispObject* LispBuiltInLetFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispEnvironment *pNewEnvironment;
	LispObject *pLocalList, *pBodyList, *pLastResult;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("let: expects at least 2 arguments (local list and body list), given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() < 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "let: expects at least 2 arguments (local list and body list), given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pLocalList = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	// rest
	pBodyList = dynamic_cast<LispCons*>(pObject)->m_pRest;

	pNewEnvironment = new LispEnvironment;
	pNewEnvironment->AddParentEnvironment(pEnvironment);


	while(!pLocalList->IsLispNil())
	{
		LispCons *pLocalNameAndValueCons;
		LispSymbol *pNameOfLocal; 
		LispObject *pLocalNameAndValue, *pExprForLocal, *pValueForLocal;

		// first
		pLocalNameAndValue = dynamic_cast<LispCons*>(pLocalList)->m_pFirst;
		// error handling
		if(!pLocalNameAndValue->IsLispCons())
		{
			return new LispError("let: local list expects name and value, given 0");
		}

		pLocalNameAndValueCons = dynamic_cast<LispCons*>(pLocalNameAndValue);
		// error handling
		if(pLocalNameAndValueCons->Length() != 2)
		{
			std::stringstream streamError;
			LispObject* pArgs = pObject;

			streamError << "let: local list expects name and value, given " << pLocalNameAndValueCons->Length() << ":";
		
			while(!pArgs->IsLispNil())
			{
				streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
				pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
			}

			return new LispError(streamError.str());
		}


		// first
		pNameOfLocal = dynamic_cast<LispSymbol*>(pLocalNameAndValueCons->m_pFirst);
		// second
		pExprForLocal = dynamic_cast<LispCons*>(pLocalNameAndValueCons->m_pRest)->m_pFirst;

		pValueForLocal = pEval->Eval(pExprForLocal, pNewEnvironment);
		// error handling
		if(pValueForLocal->IsLispError())
		{
			return pValueForLocal;
		}
		pNewEnvironment->AddBindingFor(pNameOfLocal, pValueForLocal);

		// rest
		pLocalList = dynamic_cast<LispCons*>(pLocalList)->m_pRest;
	}

	pLastResult = LispNil::GetInstance();

	while(!pBodyList->IsLispNil() &&
		  !pLastResult->IsLispError())
	{
		pLastResult = pEval->Eval(dynamic_cast<LispCons*>(pBodyList)->m_pFirst, pNewEnvironment);

		// rest
		pBodyList = dynamic_cast<LispCons*>(pBodyList)->m_pRest;
	}

	return pLastResult;
}