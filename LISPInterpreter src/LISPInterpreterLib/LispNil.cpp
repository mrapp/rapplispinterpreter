#include "StdAfx.h"
#include "LispNil.h"


LispNil* LispNil::GetInstance()
{
	static LispNil theInstanceNil;
	return &theInstanceNil;
}

bool LispNil::Equals(LispObject* pOther)
{
	return pOther->IsLispNil();
}

bool LispNil::IsLispNil()
{
	return true;
}


std::string LispNil::ToString()
{
	return "nil";
}