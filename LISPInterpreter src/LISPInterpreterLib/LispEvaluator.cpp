#include "stdafx.h"
#include "LispEvaluator.h"
#include "LispNil.h"
#include "LispTrue.h"
#include "LispFalse.h"
#include "LispFrame.h"
#include "LispInteger.h"
#include "LispByteVector.h"
#include "LispByteCodeConstants.h"
#include "LispBuiltInMinusFunction.h"
#include "LispBuiltInPlusFunction.h"
#include "LispBuiltInTimesFunction.h"
#include "LispBuiltInEqFunction.h"
#include "LispBuiltInIntegerPFunction.h"
#include "LispBuiltInConsPFunction.h"
#include "LispBuiltInFirstFunction.h"
#include "LispBuiltInRestFunction.h"
#include "LispBuiltInConsFunction.h"
#include "LispBuiltInIfFunction.h"
#include "LispBuiltInBeginFunction.h"
#include "LispBuiltInDefineFunction.h"
#include "LispBuiltInLambdaFunction.h"
#include "LispBuiltInSetFunction.h"
#include "LispBuiltInPrintFunction.h"
#include "LispBuiltInWriteFunction.h"
#include "LispBuiltInQuoteFunction.h"
#include "LispBuiltInLetFunction.h"
#include "LispBuiltInAtomPFunction.h"
#include "LispBuiltInSymbolPFunction.h"
#include "LispBuiltInNumberPFunction.h"
#include "LispBuiltInSetBytecodeFunction.h"
#include "LispBuiltInSetLiteralsFunction.h"
#include "LispBuiltInGetBodyFunction.h"
#include "LispBuiltInGetArglistFunction.h"
#include "LispBuiltInMakeByteVectorFunction.h"
#include "LispBuiltInByteVectorGetFunction.h"
#include "LispBuiltInByteVectorSetFunction.h"
#include "LispPrinter.h"
#include "LispError.h"


LispObject* LispEvaluator::DefineBuiltInFunctionsIn(LispEnvironment* pEnvironment)
{
	LispObject *pResult;
	
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("-"), new LispBuiltInMinusFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("+"), new LispBuiltInPlusFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("*"), new LispBuiltInTimesFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("eq?"), new LispBuiltInEqFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("atom?"), new LispBuiltInAtomPFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("symbol?"), new LispBuiltInSymbolPFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("number?"), new LispBuiltInNumberPFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("integer?"), new LispBuiltInIntegerPFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("cons?"), new LispBuiltInConsPFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("first"), new LispBuiltInFirstFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("rest"), new LispBuiltInRestFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("cons"), new LispBuiltInConsFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("if"), new LispBuiltInIfFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("begin"), new LispBuiltInBeginFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("define"), new LispBuiltInDefineFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("lambda"), new LispBuiltInLambdaFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("set!"), new LispBuiltInSetFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("print"), new LispBuiltInPrintFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("write"), new LispBuiltInWriteFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("quote"), new LispBuiltInQuoteFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("let"), new LispBuiltInLetFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("set-bytecode!"), new LispBuiltInSetBytecodeFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("set-literals!"), new LispBuiltInSetLiteralsFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("get-body"), new LispBuiltInGetBodyFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("get-argList"), new LispBuiltInGetArglistFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("make-byte-vector"), new LispBuiltInMakeByteVectorFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("byte-vector-get"), new LispBuiltInByteVectorGetFunction);
	pResult = pEnvironment->AddBindingFor(LispSymbol::NewLispSymbol("byte-vector-set!"), new LispBuiltInByteVectorSetFunction);

	return pResult;
}


// (eval expr env)
// returns LispError if LispObject can not be evaluated
LispObject* LispEvaluator::Eval(LispObject* pObject, LispEnvironment* pEnvironment)
{
	if(pObject->IsLispAtom())
	{
		if(pObject->IsLispSymbol())
		{
			// evaluate expr
			return pEnvironment->GetBindingFor(dynamic_cast<LispSymbol*>(pObject));
		}
		else
		{
			// expr
			return pObject;
		}
	}
	else
	{
		// function call
		LispObject *pUnevaluatedFuncSlot = dynamic_cast<LispCons*>(pObject)->m_pFirst;
		LispObject *pUnevaluatedArgs = dynamic_cast<LispCons*>(pObject)->m_pRest;
		LispObject* pEvaluatedFuncSlot = Eval(pUnevaluatedFuncSlot, pEnvironment);

		if(pEvaluatedFuncSlot->IsLispError())
		{
			return pEvaluatedFuncSlot;
		}

		if(pEvaluatedFuncSlot->IsLispBuiltInFunction())
		{
			return dynamic_cast<LispBuiltInFunction*>(pEvaluatedFuncSlot)->Evaluate(pUnevaluatedArgs, pEnvironment, this);
		}

		if(pEvaluatedFuncSlot->IsLispUserDefinedFunction())
		{
			return EvalUserDefinedFunction(dynamic_cast<LispUserDefinedFunction*>(pEvaluatedFuncSlot), pUnevaluatedArgs, pEnvironment);
		}
		
		// cannot be evaluated
		// generate error message
		return new LispError("LispEvaluator: " + pObject->ToString() + " cannot be evaluated");
	}
}



LispObject* LispEvaluator::EvalUserDefinedFunction(LispUserDefinedFunction* pLispUserDefinedFunction, LispObject* pUnevaluatedArgsInput, LispEnvironment* pEnvironment)
{
	LispEnvironment *pNewEnvironment;
	LispObject *pFormalArgs, *pNameOfFormalArg, *pUnevaluatedArgs, *pUnevaluatedArg, *pEvaluatedArg, *pBodyList, *pLastValue;
	LispFrame* pNewFrame;
	int iNumArgs, iIndex;

	pFormalArgs = pLispUserDefinedFunction->m_pArgList;
	pUnevaluatedArgs = pUnevaluatedArgsInput;

	// error handling - number arguments
	if(pFormalArgs->IsLispCons())
	{
		if(!pUnevaluatedArgs->IsLispCons())
		{
			std::stringstream streamError;

			streamError << "LispEvaluator: user-defined-function: " << pLispUserDefinedFunction->m_stFunctionName
						<< " expects " << dynamic_cast<LispCons*>(pLispUserDefinedFunction->m_pArgList)->Length()
						<< " arguments, given 0";

			return new LispError(streamError.str());
		}
		if(dynamic_cast<LispCons*>(pUnevaluatedArgs)->Length() != dynamic_cast<LispCons*>(pLispUserDefinedFunction->m_pArgList)->Length())
		{
			std::stringstream streamError;
			LispObject* pArgs = pUnevaluatedArgs;

			streamError << "LispEvaluator: user-defined-function: " << pLispUserDefinedFunction->m_stFunctionName
						<< " expects " << dynamic_cast<LispCons*>(pLispUserDefinedFunction->m_pArgList)->Length()
						<< " arguments, given " << dynamic_cast<LispCons*>(pUnevaluatedArgs)->Length() << ":";

			while(!pArgs->IsLispNil())
			{
				streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
				pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
			}

			return new LispError(streamError.str());
		}
	}

	if(!pLispUserDefinedFunction->m_pByteCode->IsLispNil())
	{
		if(pFormalArgs->IsLispNil())
		{
			iNumArgs = 0;
		}
		else
		{
			iNumArgs = dynamic_cast<LispCons*>(pFormalArgs)->Length();
		}
		pNewFrame = new LispFrame(iNumArgs + FRAMESTACKSIZE);
		pNewFrame->m_iNumArgs = iNumArgs;
		pNewFrame->m_stackPointer = iNumArgs;		// starts at 0
		pNewFrame->m_pOuterFrameOrEnvironment = pEnvironment;
		
		iIndex = 0;
		while(!pUnevaluatedArgs->IsLispNil())
		{
			pUnevaluatedArg = dynamic_cast<LispCons*>(pUnevaluatedArgs)->m_pFirst;
			pEvaluatedArg = Eval(pUnevaluatedArg, pEnvironment);
			pNewFrame->PutAt(iIndex, pEvaluatedArg);

			pUnevaluatedArgs = dynamic_cast<LispCons*>(pUnevaluatedArgs)->m_pRest;
			iIndex = iIndex + 1;
		}

		return InterpretByteCode(	dynamic_cast<LispByteVector*>(pLispUserDefinedFunction->m_pByteCode), 
									pLispUserDefinedFunction,
									pNewFrame);
	}

	pNewEnvironment = new LispEnvironment;
	pNewEnvironment->AddParentEnvironment(pLispUserDefinedFunction->m_pDefiningEnvironment);

	while(!pFormalArgs->IsLispNil())
	{
		// first
		pNameOfFormalArg = dynamic_cast<LispCons*>(pFormalArgs)->m_pFirst;

		// first
		pUnevaluatedArg = dynamic_cast<LispCons*>(pUnevaluatedArgs)->m_pFirst;

		pEvaluatedArg = Eval(pUnevaluatedArg, pEnvironment);
		// error handling
		if(pEvaluatedArg->IsLispError())
		{
			return pEvaluatedArg;
		}

		pNewEnvironment->AddBindingFor(dynamic_cast<LispSymbol*>(pNameOfFormalArg), pEvaluatedArg);

		// rest
		pFormalArgs = dynamic_cast<LispCons*>(pFormalArgs)->m_pRest;
		// rest
		pUnevaluatedArgs = dynamic_cast<LispCons*>(pUnevaluatedArgs)->m_pRest;
	}

	pBodyList = pLispUserDefinedFunction->m_pBodyList;
	pLastValue = LispNil::GetInstance();

	while(!pBodyList->IsLispNil())
	{
		pLastValue = Eval(dynamic_cast<LispCons*>(pBodyList)->m_pFirst, pNewEnvironment);

		if(pLastValue->IsLispBuiltInFunction())
		{
			return Eval(pBodyList, pNewEnvironment);
		}

		pBodyList = dynamic_cast<LispCons*>(pBodyList)->m_pRest;
	}

	return pLastValue;
}


LispObject* LispEvaluator::InterpretByteCode(LispByteVector* pByteCode, LispUserDefinedFunction* pLispUserDefinedFunction, LispFrame* pFrame)
{
	UINT32 pc = 0;
	UINT32 op;

	// iterate throught byte code stream
	while(pc < pByteCode->m_Bytes.size())
	{
		op = pc;
		pc++;

		switch(pByteCode->m_Bytes.at(op))
		{
		case BCPushArgB:
		{
			BYTE argNr = pByteCode->m_Bytes.at(pc);
			pc++;
			LispObject* pValue = pFrame->ValueAt(argNr);
			pFrame->Push(pValue);
			break;
		}
		case BCPushArgW:
		{
			// more than 256 arguments
			int argNr, tmp;
			// bit 1 to 8
			argNr = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			argNr += ((tmp << 8)	& 0xFFFFFFFF);

			LispObject* pValue = pFrame->ValueAt(argNr);
			pFrame->Push(pValue);
			break;
		}
		case BCPushNumberB:
		{
			int value = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			pFrame->Push(new LispInteger(value));
			break;
		}
		case BCPushNumberL:
		{
			int value, tmp;
			// bit 1 to 8
			value = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			value += ((tmp << 8)	& 0xFFFFFFFF);
			// bit 17 to 24
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			value += (tmp << 16)	& 0xFFFFFFFF;
			// bit 25 to 32
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			value += (tmp << 24)	& 0xFFFFFFFF;
			
			pFrame->Push(new LispInteger(value));
			break;
		}
		case BCPushGlobalB:
		{
			int litIndex = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			LispSymbol* globalName = dynamic_cast<LispSymbol*>(pLispUserDefinedFunction->GetLiteralAt(litIndex));
			LispObject* pGlobalValue = pFrame->GetBindingFor(globalName);
			if(pGlobalValue->IsLispError())
			{
				return pGlobalValue;
			}
			pFrame->Push(pGlobalValue);
			break;
		}
		case BCPushGlobalW:
		{
			// more than 256 global numbers
			int litIndex, tmp;
			// bit 1 to 8
			litIndex = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			litIndex += ((tmp << 8)	& 0xFFFFFFFF);

			LispSymbol* globalName = dynamic_cast<LispSymbol*>(pLispUserDefinedFunction->GetLiteralAt(litIndex));
			LispObject* pGlobalValue = pFrame->GetBindingFor(globalName);
			if(pGlobalValue->IsLispError())
			{
				return pGlobalValue;
			}
			pFrame->Push(pGlobalValue);
			break;
		}
		case BCPushLiteralB:
		{
			int litIndex = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			LispObject *globalName = pLispUserDefinedFunction->GetLiteralAt(litIndex);
			pFrame->Push(globalName);
			break;
		}
		case BCPushLiteralW:
		{
			// more than 256 literals
			int litIndex, tmp;
			// bit 1 to 8
			litIndex = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<int>(pByteCode->m_Bytes.at(pc));
			pc++;
			litIndex += ((tmp << 8)	& 0xFFFFFFFF);

			LispObject *globalName = pLispUserDefinedFunction->GetLiteralAt(litIndex);
			pFrame->Push(globalName);
			break;
		}
		case BCPlus:
		{
			LispObject *pObject1, *pObject2;
			int iValue1, iValue2;
			
			pObject2 = pFrame->Pop();
			pObject1 = pFrame->Pop();

			if(!pObject1->IsLispInteger())
			{
				return new LispError("LispEvaluator: First argument has to be an integer for '+'");
			}
			if(!pObject2->IsLispInteger())
			{
				return new LispError("LispEvaluator: Second argument has to be an integer for '+'");
			}

			iValue1 = dynamic_cast<LispInteger*>(pObject1)->GetValue();
			iValue2 = dynamic_cast<LispInteger*>(pObject2)->GetValue();

			pFrame->Push(new LispInteger(iValue1 + iValue2));
			break;
		}
		case BCMinus:
		{
			LispObject *pObject1, *pObject2;
			int iValue1, iValue2;
			
			pObject2 = pFrame->Pop();
			pObject1 = pFrame->Pop();

			if(!pObject1->IsLispInteger())
			{
				return new LispError("LispEvaluator: First argument has to be an integer for '-'");
			}
			if(!pObject2->IsLispInteger())
			{
				return new LispError("LispEvaluator: Second argument has to be an integer for '-'");
			}

			iValue1 = dynamic_cast<LispInteger*>(pObject1)->GetValue();
			iValue2 = dynamic_cast<LispInteger*>(pObject2)->GetValue();

			pFrame->Push(new LispInteger(iValue1 - iValue2));
			break;
		}
		case BCTimes:
		{
			LispObject *pObject1, *pObject2;
			int iValue1, iValue2;
			
			pObject2 = pFrame->Pop();
			pObject1 = pFrame->Pop();

			if(!pObject1->IsLispInteger())
			{
				return new LispError("LispEvaluator: First argument has to be an integer for '*'");
			}
			if(!pObject2->IsLispInteger())
			{
				return new LispError("LispEvaluator: Second argument has to be an integer for '*'");
			}

			iValue1 = dynamic_cast<LispInteger*>(pObject1)->GetValue();
			iValue2 = dynamic_cast<LispInteger*>(pObject2)->GetValue();

			pFrame->Push(new LispInteger(iValue1 * iValue2));
			break;
		}
		case BCEqual:
		{
			LispObject *pObject1, *pObject2;
			
			pObject2 = pFrame->Pop();
			pObject1 = pFrame->Pop();

			if(pObject1->Equals(pObject2))
			{
				pFrame->Push(LispTrue::GetInstance());
			}
			else
			{
				pFrame->Push(LispFalse::GetInstance());
			}
			break;
		}
		case BCRetTop:
			return pFrame->Pop();
			break;
		case BCPushFalse:
			pFrame->Push(LispFalse::GetInstance());
			break;
		case BCPushTrue:
			pFrame->Push(LispTrue::GetInstance());
			break;
		case BCJumpFalse:
		{
			// 32 bit (little endian)
			LispObject *pObject;
			UINT32 jumpTargetPC, tmp;
			// bit 1 to 8
			jumpTargetPC = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 8)	& 0xFFFFFFFF;
			// bit 17 to 24
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 16)	& 0xFFFFFFFF;
			// bit 25 to 32
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 24)	& 0xFFFFFFFF;

			pObject = pFrame->Pop();
			if(pObject->IsLispFalse())
			{
				pc = jumpTargetPC;
			}
			break;
		}
		case BCJumpTrue:
		{
			// 32 bit (little endian)
			LispObject *pObject;
			UINT32 jumpTargetPC, tmp;
			// bit 1 to 8
			jumpTargetPC = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 8)	& 0xFFFFFFFF;
			// bit 17 to 24
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 16)	& 0xFFFFFFFF;
			// bit 25 to 32
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 24)	& 0xFFFFFFFF;

			pObject = pFrame->Pop();
			if(pObject->IsLispTrue())
			{
				pc = jumpTargetPC;
			}
			break;
		}
		case BCJump:
		{
			UINT32 jumpTargetPC, tmp;
			// bit 1 to 8
			jumpTargetPC = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			// bit 9 to 16
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 8)	& 0xFFFFFFFF;
			// bit 17 to 24
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 16)	& 0xFFFFFFFF;
			// bit 25 to 32
			tmp = static_cast<UINT32>(pByteCode->m_Bytes.at(pc));
			pc++;
			jumpTargetPC += (tmp << 24)	& 0xFFFFFFFF;

			pc = jumpTargetPC;
			break;
		}
		case BCCall:
		{
			LispObject *pFunction, *pArg, *pReturnValue;
			BYTE numArgs;

			numArgs = pByteCode->m_Bytes.at(pc);
			pc++;

			pFunction = pFrame->OverTop(numArgs);

			// case if BuiltinFunction is not compiled
			if(pFunction->IsLispBuiltInFunction())
			{
				LispObject *pUnevaluatedArgs, *pCurrentArg;

				if(numArgs==0)
				{
					pUnevaluatedArgs = LispNil::GetInstance();
				}
				else
				{
					LispObject **args = new LispObject*[numArgs];

					// get arguments from stack and sort them in right order
					for(int i = numArgs-1; i>= 0;i--)
					{
						args[i] = pFrame->Pop();
					}

					// save arguments to LispCons
					// 1 argument
					pUnevaluatedArgs = new LispCons(args[0], LispNil::GetInstance());
					pCurrentArg = pUnevaluatedArgs;

					// 2 to n arguments
					for(int i=1; i<numArgs;i++)
					{
						dynamic_cast<LispCons*>(pCurrentArg)->m_pRest = new LispCons(args[i], LispNil::GetInstance());
						pCurrentArg = dynamic_cast<LispCons*>(pCurrentArg)->m_pRest;
					}

					// clean up
					delete[] args;
				}

				pArg = pFrame->Pop();
				pReturnValue = dynamic_cast<LispBuiltInFunction*>(pFunction)->Evaluate(pUnevaluatedArgs, pLispUserDefinedFunction->m_pDefiningEnvironment, this);
				pFrame->Push(pReturnValue);
			}
			else if(!pFunction->IsLispUserDefinedFunction())
			{
				return new LispError("LispEvaluator: Not a function call");
			}
			else if(dynamic_cast<LispUserDefinedFunction*>(pFunction)->m_pByteCode->IsLispNil())
			{
				return new LispError(	"LispEvaluator: UserDefinedFunction " + 
										dynamic_cast<LispUserDefinedFunction*>(pFunction)->m_stFunctionName + 
										" needs to be compiled first");
			}
			else
			{
				LispFrame *pNewFrame = new LispFrame(numArgs + FRAMESTACKSIZE);
				pNewFrame->m_iNumArgs = numArgs;
				pNewFrame->m_stackPointer = numArgs;
				pNewFrame->m_pOuterFrameOrEnvironment = pFrame;

				for(int i=0; i<numArgs;i++)
				{
					pArg = pFrame->Pop();
					pNewFrame->PutAt(numArgs-(i+1),pArg);
				}
				pArg = pFrame->Pop();

				pReturnValue = InterpretByteCode(pByteCode, pLispUserDefinedFunction, pNewFrame);
				pFrame->Push(pReturnValue);
			}
			break;
		}
		case BCDrop:
		{
			// pop top of frame and forget it
			LispObject *pArg = pFrame->Pop();
			break;
		}
		case BCPrint:
		{
			LispPrinter printer;

			// get result
			LispObject *pArg = pFrame->Pop();
			// print result
			std::cout << printer.Print(pArg);
			// push it back on stack
			pFrame->Push(pArg);
			break;
		}
		case BCFirst:
		{
			// pop argument 2 and forget it
			LispObject *pArg = pFrame->Pop();
			break;
		}
		default:
			// error
			return new LispError("LispEvaluator: Unknown ByteCode");
			break;
		}
	}
}