#include "stdafx.h"
#include "LispBuiltInByteVectorGetFunction.h"
#include "LispCons.h"
#include "LispInteger.h"
#include "LispByteVector.h"
#include "LispError.h"


LispObject* LispBuiltInByteVectorGetFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pByteVectorArg, *pSecondUnevaluatedArg, *pIndexArg;
	LispByteVector *pByteVector;
	int iIndexArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("byte-vector-get: expects 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "byte-vector-get: expects 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pByteVectorArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);
	// error handling
	if(pByteVectorArg->IsLispError())
	{
		return pByteVectorArg;
	}
	if(!pByteVectorArg->IsLispByteVector())
	{
		return new LispError("byte-vector-get: first argument has to be a LispByteVector");
	}

	// second
	pSecondUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->Second();
	pIndexArg = pEval->Eval(pSecondUnevaluatedArg, pEnvironment);
	// error handling
	if(pIndexArg->IsLispError())
	{
		return pIndexArg;
	}
	if(!pIndexArg->IsLispInteger())
	{
		return new LispError("byte-vector-get: second argument has to be an integer");
	}
	pByteVector = dynamic_cast<LispByteVector*>(pByteVectorArg);
	iIndexArg = dynamic_cast<LispInteger*>(pIndexArg)->GetValue();
	if(	iIndexArg < 0 ||
		iIndexArg >= static_cast<int>(pByteVector->m_Bytes.size()))
	{
		std::stringstream streamError;

		streamError << "byte-vector-get: index argument " << iIndexArg 
					<< " is out of range, range is: 0 to " << (static_cast<int>(pByteVector->m_Bytes.size())-1);

		return new LispError(streamError.str());
	}


	return new LispInteger(pByteVector->m_Bytes.at(dynamic_cast<LispInteger*>(pIndexArg)->GetValue()));
}