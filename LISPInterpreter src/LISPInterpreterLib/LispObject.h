#pragma once


#include "stdafx.h"

typedef unsigned __int8 BYTE;
typedef unsigned __int32 UINT32;

class LispObject
{
public:
	LispObject() : m_bPrintObject(false), m_bWriteObject(false) { }
	~LispObject() { }

	virtual std::string ToString() = NULL;
	virtual bool Equals(LispObject* pOther) = NULL;

	//Methods for Testing
	virtual bool IsLispAtom();
	virtual bool IsLispNumber();
	virtual bool IsLispInteger();
	virtual bool IsLispSymbol();
	virtual bool IsLispCons();
	virtual bool IsLispNil();
	virtual bool IsLispTrue();
	virtual bool IsLispFalse();
	virtual bool IsLispString();
	virtual bool IsLispBuiltInFunction();
	virtual bool IsLispUserDefinedFunction();
	virtual bool IsLispEnvironment();
	virtual bool IsLispFrame();
	virtual bool IsLispByteVector();
	virtual bool IsLispError();

	bool m_bPrintObject;
	bool m_bWriteObject;
};