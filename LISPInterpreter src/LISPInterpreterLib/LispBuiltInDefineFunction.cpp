#include "stdafx.h"
#include "LispBuiltInDefineFunction.h"
#include "LispCons.h"
#include "LispNil.h"
#include "LispUserDefinedFunction.h"
#include "LispError.h"


LispObject* LispBuiltInDefineFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pUnevaluatedNameOrFunctionDefinition, *pUnevaluatedName, *pUnevaluatedExpr, *pExprValue;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("define: expects at least 2 arguments, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() < 2)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "cons: expects at least 2 arguments, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pUnevaluatedNameOrFunctionDefinition = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	
	if(pUnevaluatedNameOrFunctionDefinition->IsLispSymbol())
	{
		pUnevaluatedName = pUnevaluatedNameOrFunctionDefinition;
		// rest first
		pUnevaluatedExpr = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pFirst;

		pExprValue = pEval->Eval(pUnevaluatedExpr, pEnvironment);
		// error handling
		if(pExprValue->IsLispError())
		{
			return pExprValue;
		}

		return pEnvironment->AddBindingFor(dynamic_cast<LispSymbol*>(pUnevaluatedName), pExprValue);
	}

	if(pUnevaluatedNameOrFunctionDefinition->IsLispCons())
	{
		LispObject *pArgList, *pBodyList;
		LispUserDefinedFunction* pNewUDF;

		// first
		pUnevaluatedName = dynamic_cast<LispCons*>(pUnevaluatedNameOrFunctionDefinition)->m_pFirst;
		// rest
		pArgList = dynamic_cast<LispCons*>(pUnevaluatedNameOrFunctionDefinition)->m_pRest;
		// rest first
		pBodyList = dynamic_cast<LispCons*>(pObject)->m_pRest;

		pNewUDF = new LispUserDefinedFunction;
		pNewUDF->m_stFunctionName = dynamic_cast<LispSymbol*>(pUnevaluatedName)->GetValue();
		pNewUDF->m_pArgList = pArgList;
		pNewUDF->m_pBodyList = pBodyList;
		pNewUDF->m_pDefiningEnvironment = pEnvironment;

		return pEnvironment->AddBindingFor(dynamic_cast<LispSymbol*>(pUnevaluatedName), pNewUDF);
	}

	return new LispError("LispBuiltInDefineFunction: function could not be defined");
}