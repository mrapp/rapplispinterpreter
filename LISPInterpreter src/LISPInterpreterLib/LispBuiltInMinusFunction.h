#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInMinusFunction : public LispBuiltInFunction
{
public:
	LispBuiltInMinusFunction() { }
	~LispBuiltInMinusFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};