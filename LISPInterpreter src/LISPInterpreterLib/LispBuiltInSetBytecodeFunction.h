#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInSetBytecodeFunction : public LispBuiltInFunction
{
public:
	LispBuiltInSetBytecodeFunction() { }
	~LispBuiltInSetBytecodeFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};