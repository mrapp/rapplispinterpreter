#include "stdafx.h"
#include "LispByteCodeAssembler.h"
#include "LispSymbol.h"
#include "LispInteger.h"


LispByteCodeAssembler::LispByteCodeAssembler()
{
	DefineAssembleFunctions();
}


std::vector<BYTE> LispByteCodeAssembler::AssembleCode(LispObject* pList)
{
	LispObject *pRestList, *pInsn;

	pRestList = pList;

	while(!pRestList->IsLispNil())
	{
		pInsn = dynamic_cast<LispCons*>(pRestList)->m_pFirst;
		AssembleInsn(pInsn);
		pRestList = dynamic_cast<LispCons*>(pRestList)->m_pRest;
	}

	BackPatchCode();

	return m_ByteCodeStream;
}


void LispByteCodeAssembler::DefineAssembleFunctions()
{
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("call", E_AssembleCall));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("equal", E_AssembleEqual));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("false_jmp", E_AssembleFalseJmp));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("jump", E_AssembleJump));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("label", E_AssembleLabel));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("minus", E_AssembleMinus));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("plus", E_AssemblePlus));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushArg", E_AssemblePushArg));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushFalse", E_AssemblePushFalse));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushGlobal", E_AssemblePushGlobal));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushNumber", E_AssemblePushNumber));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushTrue", E_AssemblePushTrue));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("pushLiteral", E_AssemblePushLiteral));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("retTop", E_AssembleRetTop));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("times", E_AssembleTimes));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("true_jmp", E_AssembleTrueJmp));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("drop", E_AssembleDrop));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("print", E_AssemblePrint));
	m_stringToFunction.insert(std::make_pair<std::string, AssembleFuncEnum>("first", E_AssembleFirst));
}


void LispByteCodeAssembler::AssembleInsn(LispObject* pInsn)
{
	LispObject* pOPArgs;
	std::string opCode;

	// first has to be a LispSymbol
	opCode = dynamic_cast<LispSymbol*>(dynamic_cast<LispCons*>(pInsn)->m_pFirst)->GetValue();
	pOPArgs = dynamic_cast<LispCons*>(pInsn)->m_pRest;

	// call corresponding assemble function
	switch(m_stringToFunction.at(opCode))
	{
	case E_AssembleCall:
		AssembleCall(pOPArgs);
		break;
	case E_AssembleEqual:
		AssembleEqual(pOPArgs);
		break;
	case E_AssembleFalseJmp:
		AssembleFalseJmp(pOPArgs);
		break;
	case E_AssembleJump:
		AssembleJump(pOPArgs);
		break;
	case E_AssembleLabel:
		AssembleLabel(pOPArgs);
		break;
	case E_AssembleMinus:
		AssembleMinus(pOPArgs);
		break;
	case E_AssemblePlus:
		AssemblePlus(pOPArgs);
		break;
	case E_AssemblePushArg:
		AssemblePushArg(pOPArgs);
		break;
	case E_AssemblePushFalse:
		AssemblePushFalse(pOPArgs);
		break;
	case E_AssemblePushGlobal:
		AssemblePushGlobal(pOPArgs);
		break;
	case E_AssemblePushNumber:
		AssemblePushNumber(pOPArgs);
		break;
	case E_AssemblePushTrue:
		AssemblePushTrue(pOPArgs);
		break;
	case E_AssemblePushLiteral:
		AssemblePushLiteral(pOPArgs);
		break;
	case E_AssembleRetTop:
		AssembleRetTop(pOPArgs);
		break;
	case E_AssembleTimes:
		AssembleTimes(pOPArgs);
		break;
	case E_AssembleTrueJmp:
		AssembleTrueJmp(pOPArgs);
		break;
	case E_AssembleDrop:
		AssembleDrop(pOPArgs);
		break;
	case E_AssemblePrint:
		AssemblePrint(pOPArgs);
		break;
	case E_AssembleFirst:
		AssembleFirst(pOPArgs);
		break;
	}
}


void LispByteCodeAssembler::AssembleCall(LispObject* pArgs)
{
	int iNumArgsInCall;

	iNumArgsInCall = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();

	m_ByteCodeStream.push_back(BCCall);
	m_ByteCodeStream.push_back(iNumArgsInCall);
}


void LispByteCodeAssembler::AssembleEqual(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCEqual);
}


void LispByteCodeAssembler::AssembleFalseJmp(LispObject* pArgs)
{
	// get label Number
	int iLabelNumber = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	// generate label
	std::stringstream ssStreamLabel; 
	ssStreamLabel << "label" << iLabelNumber; 
	PlaceJump(BCJumpFalse, ssStreamLabel.str());
}


void LispByteCodeAssembler::AssembleJump(LispObject* pArgs)
{
	int iLabelNumber = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	// generate label
	std::stringstream ssStreamLabel; 
	ssStreamLabel << "label" << iLabelNumber;

	PlaceJump(BCJump, ssStreamLabel.str());
}


void LispByteCodeAssembler::AssembleLabel(LispObject* pArgs)
{
	int iCurrentCodePosition = m_ByteCodeStream.size();

	int iLabelNumber = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	// generate label
	std::stringstream ssStreamLabel; 
	ssStreamLabel << "label" << iLabelNumber;

	std::string stLabelNr = ssStreamLabel.str();
	
	m_LabelList.insert(std::make_pair<std::string,int>(stLabelNr, iCurrentCodePosition));
}


void LispByteCodeAssembler::AssembleMinus(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCMinus);
}


void LispByteCodeAssembler::AssemblePlus(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCPlus);
}


void LispByteCodeAssembler::AssemblePushArg(LispObject* pArgs)
{
	int iArgNr = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	
	if(iArgNr <= 255)
	{
		m_ByteCodeStream.push_back(BCPushArgB);
		m_ByteCodeStream.push_back(iArgNr);
	}
	else
	{
		m_ByteCodeStream.push_back(BCPushArgW);
		CodeForShortNumber(iArgNr);
	}
}


void LispByteCodeAssembler::AssemblePushFalse(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCPushFalse);
}


void LispByteCodeAssembler::AssemblePushGlobal(LispObject* pArgs)
{
	int iGlobalNr = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	
	if(iGlobalNr <= 255)
	{
		m_ByteCodeStream.push_back(BCPushGlobalB);
		m_ByteCodeStream.push_back(iGlobalNr);
	}
	else
	{
		m_ByteCodeStream.push_back(BCPushGlobalW);
		CodeForShortNumber(iGlobalNr);
	}
}


void LispByteCodeAssembler::AssemblePushNumber(LispObject* pArgs)
{
	int iValue = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	
	// between -128 and 127
	if( iValue >= -128 && 
		iValue <= 127)
	{
		m_ByteCodeStream.push_back(BCPushNumberB);
		m_ByteCodeStream.push_back(iValue & 0xFF);
	}
	else
	{
		m_ByteCodeStream.push_back(BCPushNumberL);
		CodeForLongNumber(iValue);
	}
}


void LispByteCodeAssembler::AssemblePushTrue(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCPushTrue);
}


void LispByteCodeAssembler::AssemblePushLiteral(LispObject* pArgs)
{
	int iLiteralNr = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	
	if(iLiteralNr <= 255)
	{
		m_ByteCodeStream.push_back(BCPushLiteralB);
		m_ByteCodeStream.push_back(iLiteralNr);
	}
	else
	{
		m_ByteCodeStream.push_back(BCPushLiteralW);
		CodeForShortNumber(iLiteralNr);
	}
}


void LispByteCodeAssembler::AssembleRetTop(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCRetTop);
}


void LispByteCodeAssembler::AssembleTimes(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCTimes);
}


void LispByteCodeAssembler::AssembleTrueJmp(LispObject* pArgs)
{
	// get label Number
	int iLabelNumber = dynamic_cast<LispInteger*>(dynamic_cast<LispCons*>(pArgs)->m_pFirst)->GetValue();
	// generate label
	std::stringstream ssStreamLabel; 
	ssStreamLabel << "label" << iLabelNumber; 

	PlaceJump(BCJumpTrue, ssStreamLabel.str());
}


void LispByteCodeAssembler::AssembleDrop(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCDrop);
}


void LispByteCodeAssembler::AssemblePrint(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCPrint);
}


void LispByteCodeAssembler::AssembleFirst(LispObject* pArgs)
{
	m_ByteCodeStream.push_back(BCFirst);
}


void LispByteCodeAssembler::BackPatchCode()
{
	int iLocationToPatch, iPositionOfLabel;

	for(std::hash_multimap<std::string, int>::iterator it1 = m_BackPatchList.begin(); it1!=m_BackPatchList.end(); it1++)
	{
		// labelNr = it->first
		iLocationToPatch = it1->second;  // location where to patch in bytestream
		iPositionOfLabel = m_LabelList.at(it1->first);

		// patch location
		m_ByteCodeStream[iLocationToPatch]   = (iPositionOfLabel			& 0xFF);
		m_ByteCodeStream[iLocationToPatch+1] = ((iPositionOfLabel >> 8)		& 0xFF);
		m_ByteCodeStream[iLocationToPatch+2] = ((iPositionOfLabel >> 16)	& 0xFF);
		m_ByteCodeStream[iLocationToPatch+3] = ((iPositionOfLabel >> 24)	& 0xFF);
	}
}


void LispByteCodeAssembler::CodeForLongNumber(int iValue)
{
	m_ByteCodeStream.push_back(iValue			& 0xFF);
	m_ByteCodeStream.push_back((iValue >> 8)	& 0xFF);
	m_ByteCodeStream.push_back((iValue >> 16)	& 0xFF);
	m_ByteCodeStream.push_back((iValue >> 24)	& 0xFF);
}


void LispByteCodeAssembler::CodeForShortNumber(int iValue)
{
	m_ByteCodeStream.push_back(iValue			& 0xFF);
	m_ByteCodeStream.push_back((iValue >> 8)	& 0xFF);
}

void LispByteCodeAssembler::PlaceJump(LispByteCodeConstants byteCode, std::string stLabel)
{
	int iPosition, iPositionOfLabel;

	m_ByteCodeStream.push_back(byteCode);

	iPositionOfLabel = m_ByteCodeStream.size();

	std::hash_map<std::string,int>::iterator it = m_LabelList.find(stLabel);

	if(it != m_LabelList.end())
	{
		iPosition = it->second;
	}
	else
	{
		iPosition = 0;
	}

	CodeForLongNumber(iPosition);

	if(iPosition==0)
	{
		RememberToBackPatch(iPositionOfLabel, stLabel);
	}
}


void LispByteCodeAssembler::RememberToBackPatch(int iPositionOfLabel, std::string stLabel)
{
	std::hash_map<std::string,int>::iterator it = m_BackPatchList.find(stLabel);
	
	if(it == m_BackPatchList.end())
	{
		m_BackPatchList.insert(std::pair<std::string,int>(stLabel,iPositionOfLabel));
	}
	else
	{
		it->second = iPositionOfLabel;
	}
}