#pragma once

#include <vector>

#include "LispAtom.h"
#include "LispByteCodeConstants.h"


class LispByteVector : public LispAtom
{
public:
	std::vector<BYTE> m_Bytes;

	LispByteVector() { }
	LispByteVector(int size);
	~LispByteVector() { }

	bool IsLispByteVector();
	std::string ToString();
	bool Equals(LispObject* pOther);
};