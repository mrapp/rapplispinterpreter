#include "stdafx.h"
#include "LispBuiltInPlusFunction.h"
#include "LispCons.h"
#include "LispInteger.h"


LispObject* LispBuiltInPlusFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg, *pRestList, *pNextUnevaluatedArg, *pNextEvaluatedArg;
	int iCurrentArgument = 2;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("+: excepts 1 to n arguments, given 0");
	}

	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);

	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}
	if(!pFirstArg->IsLispInteger())
	{
		return new LispError("+: Argument 1 has to be an integer");
	}

	pRestList = dynamic_cast<LispCons*>(pObject)->m_pRest;
	int iResultValue = dynamic_cast<LispInteger*>(pFirstArg)->GetValue();

	while(!pRestList->IsLispNil())
	{
		pNextUnevaluatedArg = dynamic_cast<LispCons*>(pRestList)->m_pFirst;
		pNextEvaluatedArg = pEval->Eval(pNextUnevaluatedArg, pEnvironment);

		// error handling
		if(pNextEvaluatedArg->IsLispError())
		{
			return pNextEvaluatedArg;
		}
		if(!pNextEvaluatedArg->IsLispInteger())
		{
			std::stringstream streamError;

			streamError << "+: argument " << iCurrentArgument << " has to be an integer";

			return new LispError(streamError.str());
		}

		iResultValue = iResultValue + dynamic_cast<LispInteger*>(pNextEvaluatedArg)->GetValue();
		pRestList = dynamic_cast<LispCons*>(pRestList)->m_pRest;

		iCurrentArgument++;
	}

	return new LispInteger(iResultValue);
}