#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInLetFunction : public LispBuiltInFunction
{
public:
	LispBuiltInLetFunction() { }
	~LispBuiltInLetFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};