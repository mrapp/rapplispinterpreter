#pragma once


#include "LispAtom.h"


// Singleton
class LispTrue : public LispAtom
{
public:
	static LispTrue* GetInstance();
	bool IsLispTrue();
	std::string ToString();
	bool Equals(LispObject* pOther);

private:
	LispTrue() { }
	~LispTrue() { }

	LispTrue(const LispTrue&) { }
	LispTrue& operator=(const LispTrue&) { return *this; }
};