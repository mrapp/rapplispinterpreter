#pragma once


#include "LispAtom.h"


// Singleton
class LispFalse : public LispAtom
{
public:
	static LispFalse* GetInstance();
	bool IsLispFalse();
	std::string ToString();
	bool Equals(LispObject* pOther);

private:
	LispFalse() { }
	~LispFalse() { }

	LispFalse(const LispFalse&) { }
	LispFalse& operator=(const LispFalse&) { return *this; }
};