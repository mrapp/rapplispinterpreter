#include "stdafx.h"
#include "LispReader.h"
#include "LispString.h"
#include "LispError.h"


LispObject* LispReader::ReadStream(std::string& stStream)
{
	//delete white space at beginning
	SkipSeperators(stStream);
	
	// check stream length
	if(stStream.length() == 0)
	{
		// only whitespace -> do nothing
		return LispNil::GetInstance();
	}

	return ReadLispObject(stStream);
}


std::string LispReader::ReadFromString(std::string stString)
{
	int iPosition;

	// remove all '\n' from string
	iPosition = 0;
	iPosition = stString.find_first_of('\n', iPosition);
	
	while(iPosition != std::string::npos)
	{
		stString = stString.erase(iPosition,1);
		stString.insert(iPosition, " ");

		iPosition = stString.find_first_of('\n', iPosition);
	}

	// remove all '\t' from string
	iPosition = 0;
	iPosition = stString.find_first_of('\t', iPosition);
	
	while(iPosition != std::string::npos)
	{
		stString = stString.erase(iPosition,1);

		iPosition = stString.find_first_of('\t', iPosition);
	}

	return stString;
}


void LispReader::SkipSeperators(std::string& stStream)
{
	// find first character that is not a seperator
	int iPosition = stStream.find_first_not_of(' ', 0);

	// only seperators
	if(iPosition == std::string::npos)
	{
		stStream = "";
	}
	else
	{
		stStream = stStream.substr(iPosition);
	}
}


LispObject* LispReader::ReadLispObject(std::string& stStream)
{
	//delete white space at beginning
	SkipSeperators(stStream);
	
	//check for cons (cons a b)
	if(stStream.at(0) == '(')
	{
		return ReadLispList(stStream);
	}
	//check for '
	if(stStream.at(0) == '\'')
	{
		return ReadQuotedExpression(stStream);
	}

	return ReadLispAtom(stStream);
}


LispObject* LispReader::ReadLispList(std::string& stStream)
{
	// check stream length
	if(stStream.length() == 0)
	{
		return new LispError("LispReader: expected ')' to close '('");
	}

	// skip "("
	if(stStream.at(0) == '(')
	{
		stStream = stStream.substr(1);
	}

	//skip seperators
	SkipSeperators(stStream);

	// check stream length
	if(stStream.length() == 0)
	{
		return new LispError("LispReader: expected ')' to close '('");
	}

	if(stStream.at(0) == ')')
	{
		// skip ")"
		stStream = stStream.substr(1);

		return LispNil::GetInstance();
	}

	// quote: check for '
	if(stStream.at(0) == '\'')
	{
		stStream = stStream.substr(1);

		return ReadQuotedExpression(stStream);
	}

	LispObject* pFirst = ReadLispObject(stStream);
	if(pFirst->IsLispError())
	{
		return pFirst;
	}

	//skip seperators
	SkipSeperators(stStream);

	LispObject* pRest = ReadLispListRest(stStream);
	if(pRest->IsLispError())
	{
		return pRest;
	}

	return new LispCons(pFirst, pRest);
}


LispObject* LispReader::ReadLispListRest(std::string& stStream)
{
	// check stream length
	if(stStream.length() == 0)
	{
		return new LispError("LispReader: expected ')' to close '('");
	}

	// skip seperators
	SkipSeperators(stStream);

	if(stStream.at(0) == ')')
	{
		// skip ")"
		stStream = stStream.substr(1);

		return LispNil::GetInstance();
	}

	LispObject* pFirst = ReadLispObject(stStream);
	if(pFirst->IsLispError())
	{
		return pFirst;
	}

	// skip seperators
	SkipSeperators(stStream);

	LispObject* pRest = ReadLispListRest(stStream);
	if(pRest->IsLispError())
	{
		return pRest;
	}

	return new LispCons(pFirst, pRest);
}


LispObject* LispReader::ReadLispAtom(std::string& stStream)
{
	LispObject* pResult = NULL;

	// skip seperators
	SkipSeperators(stStream);

	// LispString?
	// check for "
	if(stStream.at(0) == '\"')
	{
		// find end "
		int iPosition = stStream.find_first_of('\"', 1);
		if(iPosition == std::string::npos)
		{
			return new LispError("LispReader: expected a closing \"");
		}

		std::string stString = stStream.substr(1, iPosition-1);

		pResult = new LispString(stString);

		// remove content from stream
		stStream = stStream.substr(pResult->ToString().length());

		return pResult;
	}

	// LispInteger?
	std::istringstream iStringStream(stStream);
	int iLispInteger = 0;
	if(iStringStream >> iLispInteger)
	{
		// ... Success!!  test is a number.
		pResult = new LispInteger(iLispInteger);

		// remove content from stream
		stStream = stStream.substr(pResult->ToString().length());

		return pResult;
	}

	// check for ' ', '.', '(', ')' und ';'
	std::string stSymbol = stStream.substr(0,stStream.find_first_of(' ',0));
	stSymbol = stSymbol.substr(0,stStream.find_first_of('.',0));
	stSymbol = stSymbol.substr(0,stStream.find_first_of('(',0));
	stSymbol = stSymbol.substr(0,stStream.find_first_of(')',0));
	stSymbol = stSymbol.substr(0,stStream.find_first_of(';',0));

	// LispTrue?
	if(stSymbol == "true")
	{
		pResult = LispTrue::GetInstance();

		// remove content from stream
		stStream = stStream.substr(pResult->ToString().length());

		return pResult;
	}

	// LispFalse?
	if(stSymbol == "false")
	{
		pResult = LispFalse::GetInstance();

		// remove content from stream
		stStream = stStream.substr(pResult->ToString().length());

		return pResult;
	}

	// LispNil?
	if(stSymbol == "nil")
	{
		pResult = LispNil::GetInstance();

		// remove content from stream
		stStream = stStream.substr(pResult->ToString().length());

		return pResult;
	}

	// has to be LispSymbol
	pResult = LispSymbol::NewLispSymbol(stSymbol);

	// remove content from stream
	stStream = stStream.substr(pResult->ToString().length());

	return pResult;
}


LispObject* LispReader::ReadQuotedExpression(std::string& stStream)
{
	LispObject* pExpression;
	// skip '
	stStream = stStream.substr(1);

	// skip end seperators
	stStream = stStream.substr(0,stStream.find_last_not_of(' ')+1);

	// check stream length
	if(stStream.length() == 0)
	{
		return new LispError("LispReader: expected an element for quoting '");
	}

	// read expression
	pExpression = ReadLispObject(stStream);

	// if '() return LispNil
	if(pExpression->IsLispNil())
	{
		return pExpression;
	}

	// first quote
	// rest	 expression
	return new LispCons(	LispSymbol::NewLispSymbol("quote"),
							new LispCons( pExpression, LispNil::GetInstance()) );
}