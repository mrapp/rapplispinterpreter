#pragma once


#include "stdafx.h"
#include "LispObject.h"
#include "LispByteCodeConstants.h"
#include "LispSymbol.h"


class LispFrame : public LispObject
{
public:
	LispFrame(int iStackSize);
	~LispFrame();

	bool IsLispFrame();
	std::string ToString();
	bool Equals(LispObject* pOther);

	LispObject* OverTop(int n);
	LispObject* Pop();
	LispObject* ValueAt(int position);
	void Push(LispObject* pValue);
	void PutAt(int iIndex, LispObject* pValue);

	LispObject* GetBindingFor(LispSymbol* pSymbol);


	LispObject* m_pOuterFrameOrEnvironment;
	int m_iNumArgs;
	int m_stackPointer;
private:
	LispObject** m_ppStackArray;
	int m_iStackSize;
};