#include "stdafx.h"
#include "LispBuiltInLambdaFunction.h"
#include "LispUserDefinedFunction.h"
#include "LispCons.h"
#include "LispNil.h"


LispObject* LispBuiltInLambdaFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("lambda: expects 'argumentlist' 'body1' and optional 'body2', given 0");
	}
	else
	{
		int iLength = dynamic_cast<LispCons*>(pObject)->Length();
		if(!(iLength == 2 || iLength == 3))
		{
			std::stringstream streamError;
			LispObject* pArgs = pObject;

			streamError << "lambda: expects 'argumentlist' 'body1' and optional 'body2', given " << iLength << ":";
		
			while(!pArgs->IsLispNil())
			{
				streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
				pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
			}

			return new LispError(streamError.str());
		}
	}

	// argumentlist body1 body2
	LispObject *pUnevaluatedArgList, *pUnevaluatedBody;
	LispUserDefinedFunction* pNewUDF;

	// first
	pUnevaluatedArgList = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	// rest first
	pUnevaluatedBody = dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pFirst;

	// rest rest
	if(!dynamic_cast<LispCons*>(dynamic_cast<LispCons*>(pObject)->m_pRest)->m_pRest->IsLispNil())
	{
		// make it a begin ...
		pUnevaluatedBody = new LispCons(LispSymbol::NewLispSymbol("begin"), dynamic_cast<LispCons*>(pObject)->m_pRest);
	}

	pNewUDF = new LispUserDefinedFunction;
	pNewUDF->m_pArgList = pUnevaluatedArgList;
	pNewUDF->m_pBodyList = pUnevaluatedBody;
	pNewUDF->m_pDefiningEnvironment = pEnvironment;

	return pNewUDF;
}