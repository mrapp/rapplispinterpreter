#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInPrintFunction : public LispBuiltInFunction
{
public:
	LispBuiltInPrintFunction() { }
	~LispBuiltInPrintFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};