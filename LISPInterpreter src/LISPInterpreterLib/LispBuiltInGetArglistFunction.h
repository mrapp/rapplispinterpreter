#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInGetArglistFunction : public LispBuiltInFunction
{
public:
	LispBuiltInGetArglistFunction() { }
	~LispBuiltInGetArglistFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};