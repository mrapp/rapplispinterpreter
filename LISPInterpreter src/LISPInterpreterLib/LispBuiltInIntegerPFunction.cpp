#include "stdafx.h"
#include "LispBuiltInIntegerPFunction.h"
#include "LispCons.h"
#include "LispTrue.h"
#include "LispFalse.h"


LispObject* LispBuiltInIntegerPFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pFirstUnevaluatedArg, *pFirstArg;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("integer?: expects 1 argument, given 0");
	}
	if(dynamic_cast<LispCons*>(pObject)->Length() != 1)
	{
		std::stringstream streamError;
		LispObject* pArgs = pObject;

		streamError << "integer?: expects 1 argument, given " << dynamic_cast<LispCons*>(pObject)->Length() << ":";
		
		while(!pArgs->IsLispNil())
		{
			streamError << " " << dynamic_cast<LispCons*>(pArgs)->m_pFirst->ToString();
			pArgs = dynamic_cast<LispCons*>(pArgs)->m_pRest;
		}

		return new LispError(streamError.str());
	}

	// first
	pFirstUnevaluatedArg = dynamic_cast<LispCons*>(pObject)->m_pFirst;
	pFirstArg = pEval->Eval(pFirstUnevaluatedArg, pEnvironment);

	// error handling
	if(pFirstArg->IsLispError())
	{
		return pFirstArg;
	}

	if(pFirstArg->IsLispInteger())
	{
		return LispTrue::GetInstance();
	}
	else
	{
		return LispFalse::GetInstance();
	}
}