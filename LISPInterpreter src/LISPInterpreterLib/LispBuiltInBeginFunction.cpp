#include "stdafx.h"
#include "LispBuiltInBeginFunction.h"
#include "LispCons.h"
#include "LispNil.h"


LispObject* LispBuiltInBeginFunction::Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval)
{
	LispObject *pRestList, *pNextUnevalutedArg, *pLastResult;

	// error handling - number arguments
	if(!pObject->IsLispCons())
	{
		return new LispError("begin: excepts 1 to n arguments, given 0");
	}

	pLastResult = LispNil::GetInstance();
	pRestList = pObject;

	while(!pRestList->IsLispNil())
	{
		// first
		pNextUnevalutedArg = dynamic_cast<LispCons*>(pRestList)->m_pFirst;
		pLastResult = pEval->Eval(pNextUnevalutedArg, pEnvironment);
		// rest
		pRestList = dynamic_cast<LispCons*>(pRestList)->m_pRest;
	}

	return pLastResult;
}