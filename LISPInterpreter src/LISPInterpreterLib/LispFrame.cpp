#include "stdafx.h"
#include "LispFrame.h"
#include "LispEnvironment.h"
#include "LispError.h"


LispFrame::LispFrame(int iStackSize)
{
	m_iStackSize = iStackSize;
	m_ppStackArray = new LispObject*[iStackSize];
	m_iNumArgs = 0;
}


LispFrame::~LispFrame()
{
	if(m_ppStackArray)
	{
		delete[] m_ppStackArray;
		m_ppStackArray = NULL;
	}
}


bool LispFrame::IsLispFrame()
{
	return true;
}


std::string LispFrame::ToString()
{
	std::stringstream ststream;

	ststream	<< "LispFrame:\n";

	for(int i=0; i < m_stackPointer; i++)
	{
		ststream << "\t" << m_ppStackArray[i]->ToString() << "\n";
	}

	return ststream.str();
}


bool LispFrame::Equals(LispObject* pOther)
{
	if(pOther->IsLispFrame())
	{
		LispFrame *pFrameOther = dynamic_cast<LispFrame*>(pOther);
		if(	m_ppStackArray == pFrameOther->m_ppStackArray)
		{
			return true;
		}

		return true;
	}

	return false;
}


LispObject* LispFrame::OverTop(int n)
{
	return m_ppStackArray[m_stackPointer-n-1];
}


LispObject* LispFrame::Pop()
{
	LispObject *pValue = m_ppStackArray[m_stackPointer-1];
	m_ppStackArray[m_stackPointer-1] = NULL;
	m_stackPointer = m_stackPointer - 1;

	return pValue;
}

LispObject* LispFrame::ValueAt(int position)
{
	return m_ppStackArray[position];
}

void LispFrame::Push(LispObject *pValue)
{
	m_ppStackArray[m_stackPointer] = pValue;
	m_stackPointer = m_stackPointer + 1;
}


void LispFrame::PutAt(int iIndex, LispObject *pValue)
{
	m_ppStackArray[iIndex] = pValue;
}


LispObject* LispFrame::GetBindingFor(LispSymbol* pSymbol)
{
	// Check parent Environment
	if(m_pOuterFrameOrEnvironment)
	{
		if(m_pOuterFrameOrEnvironment->IsLispFrame())
		{
			return dynamic_cast<LispFrame*>(m_pOuterFrameOrEnvironment)->GetBindingFor(pSymbol);
		}
		else
		{
			return dynamic_cast<LispEnvironment*>(m_pOuterFrameOrEnvironment)->GetBindingFor(pSymbol);
		}
	}
	// Symbol is not in the environment
	else
	{
		return new LispError("LispFrame: " + pSymbol->ToString() + " doesn't exist!\n");
	}
}