#pragma once


#include "LispAtom.h"


// Singleton
class LispNil : public LispAtom
{
public:
	static LispNil* GetInstance();
	bool IsLispNil();
	std::string ToString();
	bool Equals(LispObject *pOther);

private:
	LispNil() { }
	~LispNil() { }

	LispNil(const LispNil&) { }
	LispNil& operator=(const LispNil&) { return *this; }
};