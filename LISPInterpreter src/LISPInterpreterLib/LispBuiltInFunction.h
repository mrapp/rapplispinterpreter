#pragma once

#include <string>

#include "LispAtom.h"
#include "LispEnvironment.h"
#include "LispEvaluator.h"
#include "LispError.h"

class LispBuiltInFunction : public LispAtom
{
public:
	LispBuiltInFunction() { }
	~LispBuiltInFunction () { }

	virtual LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval) = NULL;
	bool IsLispBuiltInFunction();
	std::string ToString();
	bool Equals(LispObject *pOther);
};