; factorial
(define (fac n)
	(if (eq? n 1)
		1
		(* n (fac (- n 1)))))
		
(compile-userdefined-function fac)