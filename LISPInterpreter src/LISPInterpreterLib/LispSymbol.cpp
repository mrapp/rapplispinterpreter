#include "StdAfx.h"
#include "LispSymbol.h"


std::hash_map<std::string,LispSymbol*> LispSymbol::m_KnownSymbols;


LispSymbol* LispSymbol::NewLispSymbol(std::string stValue)
{
	std::hash_map<std::string,LispSymbol*>::iterator it = m_KnownSymbols.find(stValue);

	if(it == m_KnownSymbols.end())
	{
		// Symbol is not in dictionary -> create new Symbol
		LispSymbol* pNewSymbol = new LispSymbol(stValue);

		pNewSymbol->m_iReferenceCount = 1;

		m_KnownSymbols.insert(std::pair<std::string,LispSymbol*>(stValue,pNewSymbol));

		return pNewSymbol;
	}
	else
	{
		it->second->m_iReferenceCount++;

		// Symbol found
		return it->second;
	}
}


void LispSymbol::DeleteLispSymbol(LispSymbol* pSymbol)
{
	std::hash_map<std::string,LispSymbol*>::iterator it = m_KnownSymbols.find(pSymbol->m_stValue);

	if(it != m_KnownSymbols.end())
	{	
		it->second->m_iReferenceCount--;
		
		if(it->second->m_iReferenceCount < 1)
		{
			delete it->second;
			m_KnownSymbols.erase(it);
		}
	}
}


bool LispSymbol::IsLispSymbol()
{
	return true;
}


std::string LispSymbol::ToString()
{
	return m_stValue;
}


bool LispSymbol::Equals(LispObject* pOther)
{
	// when the pointer is the same it is the same LispSymbol
	return this==pOther;
}