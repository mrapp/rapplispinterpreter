#pragma once


#include "LispObject.h"


class LispAtom : public LispObject
{
public:
	LispAtom() { }
	~LispAtom() { }

	bool IsLispAtom();
};