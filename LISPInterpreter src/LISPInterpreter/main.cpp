// LISPInterpreter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "../LISPInterpreterLib/LispReader.h"
#include "../LISPInterpreterLib/LispPrinter.h"
#include "../LISPInterpreterLib/LispEvaluator.h"
#include "../LISPInterpreterLib/LispEnvironment.h"
#include "../LISPInterpreterLib/LispFileReader.h"
#include "../LISPInterpreterLib/LispCompiler.h"


using namespace std;


void PrintText(const char* fileName)
{
	string stLine;
	ifstream  fileInputStream(fileName, ios_base::in);

	if(fileInputStream.is_open())
	{
		// read file
		while(!fileInputStream.eof())
		{
			getline(fileInputStream, stLine);

			cout << stLine << endl;
		}
	}

	fileInputStream.close();
}


int _tmain(int argc, _TCHAR* argv[])
{
	// initialize LISPInterpreter
	std::string stLine;
	std::stringstream stStringStream;
	LispReader* pReader = new LispReader;
	LispPrinter* pPrinter = new LispPrinter;
	LispObject* pObject = NULL;
	LispObject* pObjectResult = NULL;
	LispEvaluator* pEval = new LispEvaluator;
	LispEnvironment* pEnv = new LispEnvironment;
	LispFileReader* pFileReader = new LispFileReader;
	LispCompiler* pCompiler= new LispCompiler;

	PrintText("..\\HelpText\\start.txt");
	PrintText("..\\HelpText\\help.txt");

	pCompiler->InitializeGlobalEnvironment(pEnv, "..\\LispFiles\\bytecode-compiler.lisp");

	// Start LISPInterpreter in console
	while(true)
	{
		cout << "> ";

		// read line
		getline(std::cin, stLine);

		if(stLine == "_quit")
		{
			break;
		}
		// load lisp file
		if(stLine.substr(0,9) == "_lispfile")
		{
			// error handling
			if(stLine.size() < 10)
			{
				cout << "LispERROR: _lispfile: no file name" << endl;
				continue;
			}
			std::string stOutput;

			pObjectResult = pFileReader->LoadAndExecuteLispFile(stLine.substr(10), stOutput, pReader, pEval, pEnv);

			if(pObjectResult->IsLispError())
			{
				cout << pObjectResult->ToString() << endl;
			}
			else
			{
				cout << stOutput << endl;
			}
			continue;
		}
		if(stLine.substr(0,13) == "_showbindings")
		{
			hash_map<LispSymbol*,LispObject*> bindings = pEnv->GetAllBindings();

			for(std::hash_map<LispSymbol*,LispObject*>::iterator it = bindings.begin(); it!=bindings.end(); it++)
			{
				cout << "\t" <<it->first->ToString() << endl;
			}

			continue;
		}
		if(stLine.substr(0,21) == "_showbuiltinfunctions")
		{
			PrintText("..\\HelpText\\builtinfunctions.txt");

			continue;
		}
		if(stLine.substr(0,5) == "_help")
		{
			PrintText("..\\HelpText\\help.txt");

			continue;
		}


		// Read
		stLine = pReader->ReadFromString(stLine);
		pObject = pReader->ReadStream(stLine);
		if(pObject->IsLispError())
		{
			// entry was not correct
			// print error message
			cout << pObject->ToString() << endl;

			delete pObject;
			pObject = NULL;

			//skip evaluation
			continue;
		}

		// Evaluate
		pObjectResult = pEval->Eval(pObject, pEnv);

		if(pObjectResult->m_bPrintObject)
		{
			pObjectResult->m_bPrintObject = false;
			cout << pPrinter->Print(pObjectResult) << endl;
		}
		else if(pObjectResult->m_bWriteObject)
		{
			pObjectResult->m_bWriteObject = false;
			pPrinter->IsWriting(true);
			cout << pPrinter->Print(pObjectResult) << endl;
			pPrinter->IsWriting(false);
		}
#ifdef _DEBUG
		// print nil only in debug version
		else if(pObjectResult)
		{
			cout << pObjectResult->ToString() << endl;
			//cout << pPrinter->Print(pObjectResult) << endl;
		}
#endif
		else if(pObjectResult &&
				!pObjectResult->IsLispNil())
		{
			cout << pObjectResult->ToString() << endl;
		}


		// Free Memory
		// delete pObjectResult
		if(	pObjectResult &&
			pObject &&
			pObject != pObjectResult &&
			!pObject->IsLispSymbol() &&					// Don't delete if pObject is LispSymbol 
			!pObject->IsLispUserDefinedFunction() &&	// or userdefined function (value of binding would be deleted)
			!pObjectResult->IsLispNil() && 
			!pObjectResult->IsLispTrue() &&
			!pObjectResult->IsLispFalse())
		{
			delete pObjectResult;
			pObjectResult = NULL;
		}
		
		// delete pObject
		if(	pObject &&
			pObject->IsLispSymbol())
		{
			LispSymbol::DeleteLispSymbol(dynamic_cast<LispSymbol*>(pObject));
			pObject = NULL;
		}
		else if(pObject &&
				!pObject->IsLispNil() && 
				!pObject->IsLispTrue() &&
				!pObject->IsLispFalse())
		{
			delete pObject;
			pObject = NULL;
		}
	}


	if(pEnv)
	{
		delete pEnv;
		pEnv = NULL;
	}
	if(pEval)
	{
		delete pEval;
		pEval = NULL;
	}
	if(pReader)
	{
		delete pReader;
		pReader = NULL;
	}
	if(pPrinter)
	{
		delete pPrinter;
		pPrinter = NULL;
	}
	if(pFileReader)
	{
		delete pFileReader;
		pFileReader = NULL;
	}
	if(pCompiler)
	{
		delete pCompiler;
		pCompiler = NULL;
	}

	return 0;
}

