#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInIntegerPFunction : public LispBuiltInFunction
{
public:
	LispBuiltInIntegerPFunction() { }
	~LispBuiltInIntegerPFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};