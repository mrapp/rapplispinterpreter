(define (make-point x y)
	(begin
		(define (get-x)
			x)
		(define (get-y)
			y)
		(define (set-x! newX)
			(set! x newX))
		(define (set-y! newY)
			(set! y newY))
		(define (area)
			(* x y))
		(define (error)
			(print "mist"))

		(define (dispatch op)
			(if (eq? op (quote get-x))
				get-x
			(if (eq? op (quote get-y))
				get-y
			(if (eq? op (quote set-x!))
				set-x!
			(if (eq? op (quote set-y!))
				set-y!
			(if (eq? op (quote area))
				area
				error))))))

	dispatch)
)