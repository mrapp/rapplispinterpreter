
Commands
  _lispfile <file>
    load and execute LispFile
    <file>
      lisp file to execute with relative file path or full file path
        -relative path ..\folder\*.lisp
        -full path C:\folder\*.lisp
  _showbindings
    show all bindings
  _showbuiltinfunctions
    show all built-in functions
  _help
    show all commands
  _quit
    close the application
