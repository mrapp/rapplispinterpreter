#pragma once


#include "LispBuiltInFunction.h"


class LispBuiltInConsPFunction : public LispBuiltInFunction
{
public:
	LispBuiltInConsPFunction() { }
	~LispBuiltInConsPFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};