#pragma once

#include "LispBuiltInFunction.h"


class LispBuiltInPlusFunction : public LispBuiltInFunction
{
public:
	LispBuiltInPlusFunction() { }
	~LispBuiltInPlusFunction () { }

	LispObject* Evaluate(LispObject* pObject, LispEnvironment* pEnvironment, LispEvaluator* pEval);
};